<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tracer extends Model
{
    protected $table = "tracer";
    protected $fillable =['title','description','file','link','status'];
}

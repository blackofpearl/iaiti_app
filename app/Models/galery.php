<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class galery extends Model
{
    protected $table = "galery";
    protected $fillable =['kategori_id','title','description','file','status'];
}

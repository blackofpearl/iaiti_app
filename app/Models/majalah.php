<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class majalah extends Model
{
    protected $table = "majalah";
    protected $fillable =['title','description','file','link','status'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class reg_mubes extends Model
{
    protected $table = "reg_mubes";
    protected $fillable =['id_user','nim','name','prodi','angkatan','phone','tgl_masuk','tgl_lulus','kelas','extra_a','extra_b','kode','status','file'];
}

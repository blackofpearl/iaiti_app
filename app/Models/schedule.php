<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class schedule extends Model
{
    protected $table = "schedule";
    protected $fillable =['title','periode_a','periode_b','status','extra_a','extra_b','extra_c'];
}

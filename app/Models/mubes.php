<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mubes extends Model
{
    protected $table = "mubes";
    protected $fillable =['title','description'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class alumni extends Model
{
    protected $table = "alumni_iti";
    protected $fillable =['NIM','Nama','Status','Tanggal Masuk', 'Tanggal Lulus','Semester', 'Thn Akademik','Prodi','Kelas'];
}

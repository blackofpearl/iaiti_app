<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class savedel extends Model
{
    protected $table = "savedel";
    protected $fillable =['nim','email','nama','del','actor_del','email_del'];
}

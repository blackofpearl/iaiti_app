<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class log_activity extends Model
{
    protected $table = "log_activity";
    protected $fillable =['id_user','name','email','date_log','ip','status','menu','url','keterangan','extra_a','extra_b'];
}

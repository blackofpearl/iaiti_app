<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ip_cek extends Model
{
    protected $table = "ip_cek";
    protected $fillable =['ip','date_ip','position','user','status'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class candidate extends Model
{
    protected $table = "candidate";
    protected $fillable =['id_alumni','id_sch','ms','vs','file','extra_a','extra_b','extra_c'];
}

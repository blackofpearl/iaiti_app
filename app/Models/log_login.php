<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class log_login extends Model
{
    protected $table = "log_login";
    protected $fillable =['id_user','date_log','ip','extra_a','extra_b'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kontak extends Model
{
    protected $table = "kontak";
    protected $fillable =['title','address','description','gmaps','email','phone','fax','copyright','design','fb','twitter','instagram','linkedin'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class about extends Model
{
    
    protected $table = "about";
    protected $fillable =['desc_remaks','desc_a','desc_b','youtube','desc_vm','visi','misi','file'];
}

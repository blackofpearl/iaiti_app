<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\listuser;
use Illuminate\Support\Facades\Mail;
use App\Mail\iaitiMail;

class listuserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function profil()
    {
      $id = \Auth::user()->id;
      $role = \DB::table('user_roles')
      ->join('users', 'users.id', '=', 'user_roles.id_user')
      ->join('roles', 'user_roles.id_role', '=',  'roles.id')
      ->select('users.name as name','users.email as email', 'roles.role_user as roles_name','roles.id as id_role', 'users.id as id')
      ->where('users.id', '=', $id)
      ->get();
        
      return view('admin.profil.index',['role'=>$role]);
    }  

    public function index()
    {
		$role = \DB::table('users')
            ->join('user_roles','users.id','=','user_roles.id_user')
            ->join('roles','roles.id','=','user_roles.id_role')
            ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user')
            ->where('roles.id', '<', '4')
            ->paginate(10);
			
		return view('admin.listuser.index',['role'=>$role]);
    }    

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$role =\DB::table('users')
    ->join('user_roles','users.id','=','user_roles.id_user')
    ->join('roles','roles.id','=','user_roles.id_role')
    ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user')
		->where('users.name','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('admin.listuser.index',['role'=>$role]);
 
	}

  public function cari2(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
        $role = \DB::table('users')
        ->join('reg_mubes','reg_mubes.id_user','=','users.id')
            ->join('user_roles','users.id','=','user_roles.id_user')
            ->join('roles','roles.id','=','user_roles.id_role')
            ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                      , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                      , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
    ->where('roles.id', '>', '3')
		->where('users.name','like',"%".$cari."%")
		->orWhere('reg_mubes.nim','like',"%".$cari."%")
    ->orderBy('reg_mubes.id','DESC')
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('admin.listuser2.index2',['role'=>$role]);
 
	}

    public function index2()
    {
			$role = \DB::table('users')
      ->join('reg_mubes','reg_mubes.id_user','=','users.id')
      ->join('user_roles','users.id','=','user_roles.id_user')
      ->join('roles','roles.id','=','user_roles.id_role')
      ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                      , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                      , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
            ->where('roles.id', '>', '3')
            ->orderBy('reg_mubes.id','DESC')
            ->paginate(6);
			
		return view('admin.listuser2.index2',['role'=>$role]);
    }    

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('user_roles')
    ->join('users', 'users.id', '=', 'user_roles.id_user')
    ->join('roles', 'user_roles.id_role', '=',  'roles.id')
    ->select('users.name as name','users.email as email', 'roles.role_user as roles_name','roles.id as id_role', 'users.id as id')
    ->where('users.id', '=', $id)
    ->get();
			
		return view('admin.listuser.edit',['role'=>$role]);
    }    

    //edit2
    public function edit2(Request $request, $id)
    {
			$role = \DB::table('users')
            ->join('reg_mubes','reg_mubes.id_user','=','users.id')
            ->join('user_roles','users.id','=','user_roles.id_user')
            ->join('roles','roles.id','=','user_roles.id_role')
            ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                      , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                      , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
    ->where('users.id', '=', $id)
    ->get();
			
		return view('admin.listuser2.edit2',['role'=>$role]);
    }    
    
    public function add()
    {
        return view('admin.listuser.add');
    }

    public function add2()
    {
        return view('admin.listuser2.add2');
    }

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('users')
            ->select('id','name','email')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.listuser.show',['role'=>$role]);
    }    

    public function destroy($id)
    {
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'List Users - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data List Users';
      $log->save();

      \DB::delete('delete from user_roles where id_user = ?', [$id]);

      \DB::delete('delete from users where id = ?', [$id]);
      
      return redirect('/admin/listuser')->with('success', 'Data Berhasil Dihapus'); 

    }

    public function destroy2($id)
    {

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'List Users - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data List Users';
      $log->save();

      $det = \DB::table('users')
      ->join('reg_mubes','reg_mubes.id_user','=','users.id')
      ->select('reg_mubes.nim as nim', 'users.name as name', 'users.email')
      ->where('users.id', '=', $id)
      ->get();
      foreach($det as $cek)
      {
      date_default_timezone_set("Asia/Jakarta");
      $hap = new \App\Models\savedel;
      $hap->nim = $cek->nim;
      $hap->nama = $cek->name;
      $hap->email = $cek->email;
      $hap->actor_del = \Auth::user()->name;
      $hap->email_del = \Auth::user()->email;
      $hap->del = date('Y-m-d H:i:s');
      $hap->save();
      }
      
        \DB::delete('delete from user_roles where id_user = ?', [$id]);
  
        \DB::delete('delete from reg_mubes where id_user = ?', [$id]);
  
        \DB::delete('delete from users where id = ?', [$id]);

      return redirect('/admin/listuser2')->with('success', 'Data Berhasil Dihapus'); 
    }

    //Save listuser
    public function create(Request $request)
    {

      
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'List Users - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data List Users';
      $log->save();

      $hit = \DB::table('users')->where('email', '=', $request->email)->get();
        $hit = $hit->count();

        if($hit > 0){

          return redirect()->back()->withErrors(['Error', 'Email Sudah Terdaftar']);

        }else{

          
      $pass=$request->password;
      $uppercase = preg_match('@[A-Z]@', $pass);
      $lowercase = preg_match('@[a-z]@', $pass);
      $number    = preg_match('@[0-9]@', $pass);

      if(!$uppercase || !$lowercase || !$number || strlen($pass) <=6 ){

        return redirect()->back()->withErrors(['Error', 'Password Wajib minimum 8 Character dan mengandung huruf BESAR, huruf kecil dan angka']);

      }else{

        $user = new \App\Models\listuser;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->remember_token = bcrypt('1234');
        $user->save();
    
        $rolesa = new \App\Models\user_roles;
        $rolesa->id_user = $user->id;
        $rolesa->id_role = $request->id_role;
        $rolesa->save();

        return redirect('/admin/listuser')->with('success', 'Data Berhasil Disimpan'); 


      }

        }
                   
    }

    //Save listuser
    public function create2(Request $request)
    {

      
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'List Users - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data List Users';
      $log->save();

      function alphanumeric($panjang)
  {
          $data = array(

              range(0, 9),
              range('a', 'z'),
              range('A','Z')

          );

          $karakter = array();
          foreach ($data as $key => $value) {
              foreach ($value as $k => $v) {
                  $karakter[]=$v;
              }
          }
          $data = null;

          for($i=0; $i<=$panjang; $i++)
          {
              $data .=$karakter[rand($i, count($karakter)-1)];

          }
          return $data;
  } 
      $hit = \DB::table('users')->where('email', '=', $request->email)->get();
        $hit = $hit->count();

        if($hit > 0){

          return redirect()->back()->withErrors(['Error', 'Email Sudah Terdaftar']);

        }else{

          
      $pass=$request->password;
      $uppercase = preg_match('@[A-Z]@', $pass);
      $lowercase = preg_match('@[a-z]@', $pass);
      $number    = preg_match('@[0-9]@', $pass);

      if(!$uppercase || !$lowercase || !$number || strlen($pass) <=6 ){

        return redirect()->back()->withErrors(['Error', 'Password Wajib minimum 8 Character dan mengandung huruf BESAR, huruf kecil dan angka']);

      }else{

        $validator = Validator::make($request->all(), [
          'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048',
      ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan Images(jpg,png,jpeg,gif,svg) atau file ukuran melebihi 10000']);  
         }

        $user = new \App\Models\listuser;
        $user->name = $request->username;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->remember_token = bcrypt('1234');
        $user->save();
    
        $rolesa = new \App\Models\user_roles;
        $rolesa->id_user = $user->id;
        $rolesa->id_role = $request->id_role;
        $rolesa->save();

        $file = $request->file('file');
                   
                   $filenameWithExt = $request->file('file')->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                   $extension = $request->file('file')->getClientOriginalExtension();
                   $filenameSimpan = $filename.'_'.time().'.'.$extension;
                   $path = $request->file('file')->storeAs('/public/profil', $filenameSimpan);
           
                   $reg = new \App\Models\reg_mubes;
                   $reg->id_user = $user->id;
                   $reg->nim = $request->nim;
                   $reg->name = $request->name;
                   $reg->prodi = $request->prodi;
                   $reg->extra_a = $request->extra_a;
                   $reg->extra_b = $request->extra_b;
                   $reg->phone = $request->phone1.''.$request->phone2.''.$request->phone3;
                   $reg->angkatan = $request->angkatan;
                   $reg->kode = alphanumeric(6);
                   $reg->status = '2';
                   $reg->file = $filenameSimpan;
                   $reg->save();

                   $details = [
                    'title' => 'Register Success',
                    'body' => 'Status Akun Telah Di Buatkan By System, Silahkan Tunggu Informasi Selanjutnya',
                    'username' => $request->username,
                    'nim' => $request->nim,
                    'kode' => alphanumeric(6)
                     ];
        
                    Mail::to($request->email)->send(new iaitiMail($details));

        return redirect('/admin/listuser2')->with('success', 'Data Berhasil Disimpan'); 


      }

        }
                   
    }

    //update listuser
    public function update(Request $request, $id)
    {
      $from = \DB::table('users')
            ->select('*')
            ->where('id','=', $id)
            ->get();

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Update';
      $log->menu = 'List Users - Update';
      $log->url = url()->full();
      $log->keterangan = 'Update data List Users : nama : '.$request->name.'
       - Email : '.$request->email.' - Sebelumnya : '.$from;
      $log->save();

      //cek password ada tidak
      if(empty($request->password)){

        $listuser = \App\Models\listuser::findOrFail($id);
             \DB::table('users')->where('id',$id)->update([
                'name' => $request->name,
                'email' => $request->email
                 ]);
        
        $rolesa = \App\Models\user_roles::findOrFail($request->id_role);
                 \DB::table('user_roles')->where('id_user',$id)->update([
                    'id_role' => $request->id_role
                     ]);
                     
        return redirect('/admin/listuser')->with('success', 'Data Berhasil Update'); 

      }else{

        $pass=$request->password;
      $uppercase = preg_match('@[A-Z]@', $pass);
      $lowercase = preg_match('@[a-z]@', $pass);
      $number    = preg_match('@[0-9]@', $pass);

      if(!$uppercase || !$lowercase || !$number || strlen($pass) <=6 ){

        return redirect()->back()->withErrors(['Error', 'Password Wajib minimum 8 Character dan mengandung huruf BESAR, huruf kecil dan angka']);

      }else{

        $listuser = \App\Models\listuser::findOrFail($id);
             \DB::table('users')->where('id',$id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => md5($request->password)
                 ]);
        
        $rolesa = \App\Models\user_roles::findOrFail($request->id_role);
                 \DB::table('user_roles')->where('id_user',$id)->update([
                    'id_role' => $request->id_role
                     ]);

        return redirect('/admin/listuser')->with('success', 'Data Berhasil Update'); 
      }

      }

    }

    //update listuser
    public function update2(Request $request, $id)
    {
      
      $from = \DB::table('users')
            ->select('*')
            ->where('id','=', $id)
            ->get();

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Update';
      $log->menu = 'List Users - Update';
      $log->url = url()->full();
      $log->keterangan = 'Update data List Users : nama : '.$request->name.'
       - Email : '.$request->email.' - Sebelumnya : '.$from;
      $log->save();
      //cek images kosong
      if(empty($request->file)){
       
        //password kosong
        if(empty($request->password)){
          $listuser = \App\Models\listuser::findOrFail($id);
               \DB::table('users')->where('id',$id)->update([
                  'name' => $request->username,
                  'email' => $request->email
                   ]);

          $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$id)->update([
                          'nim' => $request->nim,
                          'name' => $request->name,
                          'prodi' => $request->prodi,
                          'phone' => $request->phone1.''.$request->phone2.''.$request->phone3,
                          'status' => $request->status,
                          'extra_a' => $request->extra_a,
                          'extra_b' => $request->extra_b,
                          'angkatan' => $request->angkatan
                           ]);

                           if($request->status == 2){
                            $details = [
                              'title' => 'Register Success',
                              'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => $request->kode
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else if($request->status == 3){
                            $details = [
                              'title' => 'Register Failed',
                              'body' => 'Status Akun Tidak Di Setujui',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Data Anda Di Tolak'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else{
                            $details = [
                              'title' => 'Register Waitting',
                              'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Mohon Di Tunggu Informasinya'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
                          }
                       
          return redirect('/admin/listuser2')->with('success', 'Data Berhasil Update');

         //password isi 
        }else{
          
        $pass=$request->password;
        $uppercase = preg_match('@[A-Z]@', $pass);
        $lowercase = preg_match('@[a-z]@', $pass);
        $number    = preg_match('@[0-9]@', $pass);
  
        if(!$uppercase || !$lowercase || !$number || strlen($pass) <=6 ){
  
          return redirect()->back()->withErrors(['Error', 'Password Wajib minimum 8 Character dan mengandung huruf BESAR, huruf kecil dan angka']);
  
        }else{
  
          $listuser = \App\Models\listuser::findOrFail($id);
               \DB::table('users')->where('id',$id)->update([
                  'name' => $request->username,
                  'email' => $request->email,
                  'password' => md5($request->password)
                   ]);

                       $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$id)->update([
                          'nim' => $request->nim,
                          'name' => $request->name,
                          'prodi' => $request->prodi,
                          'phone' => $request->phone1.''.$request->phone2.''.$request->phone3,
                          'status' => $request->status,
                          'extra_a' => $request->extra_a,
                          'extra_b' => $request->extra_b,
                          'angkatan' => $request->angkatan
                           ]);

                           if($request->status == 2){
                            $details = [
                              'title' => 'Register Success',
                              'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => $request->kode
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else if($request->status == 3){
                            $details = [
                              'title' => 'Register Failed',
                              'body' => 'Status Akun Tidak Di Setujui',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Data Anda Di Tolak'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else{
                            $details = [
                              'title' => 'Register Waitting',
                              'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Mohon Di Tunggu Informasinya'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
                          }
  
          return redirect('/admin/listuser2')->with('success', 'Data Berhasil Update'); 
        }
  
        }

      //jika images ada
      }else{

        $validator = Validator::make($request->all(), [
          'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048',
      ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan Images(jpg,png,jpeg,gif,svg) atau file ukuran melebihi 10000']);  
         }

        //password kosong
        if(empty($request->password)){
          $listuser = \App\Models\listuser::findOrFail($id);
               \DB::table('users')->where('id',$id)->update([
                  'name' => $request->username,
                  'email' => $request->email
                   ]);
         

			$file = $request->file('file');
                   
                   $filenameWithExt = $request->file('file')->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                   $extension = $request->file('file')->getClientOriginalExtension();
                   $filenameSimpan = $filename.'_'.time().'.'.$extension;
                   $path = $request->file('file')->storeAs('/public/profil', $filenameSimpan);
			
          $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$id)->update([
                          'nim' => $request->nim,
                          'name' => $request->name,
                          'prodi' => $request->prodi,
                          'phone' => $request->phone1.''.$request->phone2.''.$request->phone3,
                          'status' => $request->status,
                          'extra_a' => $request->extra_a,
                          'extra_b' => $request->extra_b,
                          'angkatan' => $request->angkatan,
                          'file' => $filenameSimpan
                           ]);

                           if($request->status == 2){
                            $details = [
                              'title' => 'Register Success',
                              'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => $request->kode
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else if($request->status == 3){
                            $details = [
                              'title' => 'Register Failed',
                              'body' => 'Status Akun Tidak Di Setujui',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Data Anda Di Tolak'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else{
                            $details = [
                              'title' => 'Register Waitting',
                              'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Mohon Di Tunggu Informasinya'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
                          }
                       
          return redirect('/admin/listuser2')->with('success', 'Data Berhasil Update');

         //password isi 
        }else{
          
          $pass=$request->password;
        $uppercase = preg_match('@[A-Z]@', $pass);
        $lowercase = preg_match('@[a-z]@', $pass);
        $number    = preg_match('@[0-9]@', $pass);
  
        if(!$uppercase || !$lowercase || !$number || strlen($pass) <=6 ){
  
          return redirect()->back()->withErrors(['Error', 'Password Wajib minimum 8 Character dan mengandung huruf BESAR, huruf kecil dan angka']);
  
        }else{
  
			$listuser = \App\Models\listuser::findOrFail($id);
               \DB::table('users')->where('id',$id)->update([
                  'name' => $request->username,
                  'email' => $request->email,
                  'password' => md5($request->password)
                   ]);

			$file = $request->file('file');
                   
                   $filenameWithExt = $request->file('file')->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                   $extension = $request->file('file')->getClientOriginalExtension();
                   $filenameSimpan = $filename.'_'.time().'.'.$extension;
                   $path = $request->file('file')->storeAs('/public/profil', $filenameSimpan);
			
          $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$id)->update([
                          'nim' => $request->nim,
                          'name' => $request->name,
                          'prodi' => $request->prodi,
                          'phone' => $request->phone1.''.$request->phone2.''.$request->phone3,
                          'status' => $request->status,
                          'extra_a' => $request->extra_a,
                          'extra_b' => $request->extra_b,
                          'angkatan' => $request->angkatan,
                          'file' => $filenameSimpan
                           ]);

                           if($request->status == 2){
                            $details = [
                              'title' => 'Register Success',
                              'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => $request->kode
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else if($request->status == 3){
                            $details = [
                              'title' => 'Register Failed',
                              'body' => 'Status Akun Tidak Di Setujui',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Data Anda Di Tolak'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else{
                            $details = [
                              'title' => 'Register Waitting',
                              'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Mohon Di Tunggu Informasinya'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
                          }
  
          return redirect('/admin/listuser2')->with('success', 'Data Berhasil Update'); 
        }

        }
        
      }

    }

     //update listuser
     public function update_pro(Request $request, $id)
     {
      $from = \DB::table('users')
      ->select('*')
      ->where('id','=', $id)
      ->get();

$ip=\request()->ip();
date_default_timezone_set("Asia/Jakarta");
$log = new \App\Models\log_activity;
$log->id_user = \Auth::user()->id;
$log->name = \Auth::user()->name;
$log->email = \Auth::user()->email;
$log->date_log = date('Y-m-d H:i:s');
$log->ip = $ip;
$log->status = 'Update';
$log->menu = 'List Users - Update';
$log->url = url()->full();
$log->keterangan = 'Update data List Users : nama : '.$request->name.'
 - Email : '.$request->email.' - Sebelumnya : '.$from;
$log->save();

       //cek password ada tidak
       if(empty($request->password)){
 
         $listuser = \App\Models\listuser::findOrFail($id);
              \DB::table('users')->where('id',$id)->update([
                 'name' => $request->name,
                 'email' => $request->email
                  ]);
         
         $rolesa = \App\Models\user_roles::findOrFail($request->id_role);
                  \DB::table('user_roles')->where('id_user',$id)->update([
                     'id_role' => $request->id_role
                      ]);
                      
         return redirect('/admin/profil')->with('success', 'Data Berhasil Update'); 
 
       }else{
 
         $pass=$request->password;
       $uppercase = preg_match('@[A-Z]@', $pass);
       $lowercase = preg_match('@[a-z]@', $pass);
       $number    = preg_match('@[0-9]@', $pass);
 
       if(!$uppercase || !$lowercase || !$number || strlen($pass) <=6 ){
 
         return redirect()->back()->withErrors(['Error', 'Password Wajib minimum 8 Character dan mengandung huruf BESAR, huruf kecil dan angka']);
 
       }else{
 
         $listuser = \App\Models\listuser::findOrFail($id);
              \DB::table('users')->where('id',$id)->update([
                 'name' => $request->name,
                 'email' => $request->email,
                 'password' => md5($request->password)
                  ]);
         
         $rolesa = \App\Models\user_roles::findOrFail($request->id_role);
                  \DB::table('user_roles')->where('id_user',$id)->update([
                     'id_role' => $request->id_role
                      ]);
 
         return redirect('/admin/profil')->with('success', 'Data Berhasil Update'); 
       }
 
       }
 
     }

}

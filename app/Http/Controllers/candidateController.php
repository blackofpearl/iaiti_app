<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\candidate;

class candidateController extends Controller
{
         /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
		$role = \DB::table('candidate')
            ->select('*')
            ->paginate(10);
			
		return view('admin.kandidat.index',['role'=>$role]);
    }    

    public function add()
    {
        return view('admin.kandidat.add');
    }

    //Save candidate
    public function create(Request $request)
    {
		
		$validator = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
        }
		
		$file = $request->file('file');
		
		$filenameWithExt = $request->file('file')->getClientOriginalName();
		$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
		$extension = $request->file('file')->getClientOriginalExtension();
		$filenameSimpan = $filename.'_'.time().'.'.$extension;
		$path = $request->file('file')->storeAs('/public/candidate', $filenameSimpan);

        $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Create';
            $log->menu = 'Candidate - Create';
            $log->url = url()->full();
            $log->keterangan = 'Menambahkan Candidate : '.$request->id_alumni.'(id) | '.$filenameSimpan.'(Foto)';
            $log->save();

		$candidate = new \App\Models\candidate;
        $candidate->id_alumni = $request->id_alumni;
        $candidate->id_sch = $request->id_sch;
        $candidate->vs = $request->vs;
        $candidate->ms = $request->ms;
        $candidate->file = $filenameSimpan;
        $candidate->save();

        
		return redirect('/admin/kandidat')->with('sukses','Data Berhasil Buat');
	}

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('candidate')
        ->join('schedule', 'schedule.id', '=', 'candidate.id_sch')
        ->join('alumni_iti', 'alumni_iti.id', '=', 'candidate.id_alumni')
        ->select('candidate.id as id','candidate.id_alumni as id_alumni','candidate.id_sch as id_sch','candidate.vs as vs', 'candidate.ms as ms', 'candidate.file as file')
        ->where('candidate.id', '=', $id)
        ->get();
			
		return view('admin.kandidat.edit',['role'=>$role]);
    }    

    public function update(Request $request, $id)
    {
		
		$validator = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){
            $from = \DB::table('candidate')
            ->select('*')
            ->where('id','=', $id)
            ->get();

            $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Update';
            $log->menu = 'Candidate - Update';
            $log->url = url()->full();
            $log->keterangan = 'Update Candidate : '.$request->id_alumni.'(id) | Diganti : '.$from.'(Sebelumnya)';
            $log->save();
            $candidate = \App\Models\candidate::findOrFail($id);
            \DB::table('candidate')->where('id',$id)->update([
                'id_alumni' => $request->id_alumni,
                'id_sch' => $request->id_sch,
                'vs' => $request->vs,
                'ms' => $request->ms
                ]);
               
                return redirect('/admin/kandidat')->withSuccess('success.');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/candidate', $filenameSimpan);

            $from = \DB::table('candidate')
            ->select('*')
            ->where('id','=', $id)
            ->get();

            $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Update';
            $log->menu = 'Candidate - Update';
            $log->url = url()->full();
            $log->keterangan = 'Update Candidate : '.$request->id_alumni.'(id)'.$filenameSimpan.' | Diganti : '.$from.'(Sebelumnya)';
            $log->save();

            $candidate = \App\Models\candidate::findOrFail($id);
            \DB::table('candidate')->where('id',$id)->update([
                'id_alumni' => $request->id_alumni,
                'id_sch' => $request->id_sch,
                'vs' => $request->vs,
                'ms' => $request->ms,
                'file' => $filenameSimpan
                ]);
               
                return redirect('/admin/kandidat')->withSuccess('success.');

        }

	}

    public function destroy($id)
        {
        
            $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Delete';
            $log->menu = 'Candidate - Delete';
            $log->url = url()->full();
            $log->keterangan = 'Menghapus Candidate : '.$id.'(id)';
            $log->save();

        \DB::delete('delete from vote where id_cdt = ?', [$id]);
        $candidate = \App\Models\candidate::findOrFail($id);
        $candidate->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
        }

}

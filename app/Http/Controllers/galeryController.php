<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\galery;

class galeryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('galery')
            ->join('kategori','kategori.id','=','galery.kategori_id')
            ->select('kategori.name as kat_name','galery.id as id','galery.kategori_id as kategori_id','galery.title', 'galery.description', 'galery.status','galery.file','galery.created_at')
            ->paginate(2);
			
		return view('admin.galeri.index',['role'=>$role]);
    }    

    //Add galery
    public function add()
    {
        return view('admin.galeri.add');
    }

    //Save galery
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){
           
            $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Create';
            $log->menu = 'Galery - Create';
            $log->url = url()->full();
            $log->keterangan = 'Menambahkan Galery Tanpa Images';
            $log->save();

           $galery = new \App\Models\galery;
           $galery->kategori_id = $request->kategori_id;
           $galery->title = $request->title;
           $galery->description = $request->description;
           $galery->status = $request->status;
           $galery->save();
   
           
           return redirect('/admin/galery')->with('sukses','Data Berhasil Buat');
        }else{
           if ($validator->fails()) {
               return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan Images atau file ukuran melebihi 10000']);  
           }
           
           $file = $request->file('file');
           
           $filenameWithExt = $request->file('file')->getClientOriginalName();
           $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
           $extension = $request->file('file')->getClientOriginalExtension();
           $filenameSimpan = $filename.'_'.time().'.'.$extension;
           $path = $request->file('file')->storeAs('/public/galery', $filenameSimpan);
           
           $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Create';
            $log->menu = 'Galery - Create';
            $log->url = url()->full();
            $log->keterangan = 'Menambahkan Galery dengan images : '.$filenameSimpan;
            $log->save();

           $galery = new \App\Models\galery;
           $galery->kategori_id = $request->kategori_id;
           $galery->title = $request->title;
           $galery->description = $request->description;
           $galery->status = $request->status;
           $galery->file = $filenameSimpan;
           $galery->save();
   
           
           return redirect('/admin/galery')->with('sukses','Data Berhasil Buat');
        }

       
    }

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('galery')
        ->join('kategori','kategori.id','=','galery.kategori_id')
        ->select('kategori.name as kat_name','galery.id as id','galery.kategori_id as kategori_id','galery.title', 'galery.description', 'galery.status','galery.file','galery.created_at')
        ->where('galery.id', '=', $id)
            ->get();
			
		return view('admin.galeri.show',['role'=>$role]);
    }    

    public function destroy($id)
    {

        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $log = new \App\Models\log_activity;
        $log->id_user = \Auth::user()->id;
        $log->name = \Auth::user()->name;
        $log->email = \Auth::user()->email;
        $log->date_log = date('Y-m-d H:i:s');
        $log->ip = $ip;
        $log->status = 'Delete';
        $log->menu = 'Galery - Delete';
        $log->url = url()->full();
        $log->keterangan = 'Menghapus data Galery';
        $log->save();
        
    $galery = \App\Models\galery::findOrFail($id);
    $galery->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

     //update galery
     public function update(Request $request, $id)
     {
         
         $validator = Validator::make($request->all(), [
             'title' => 'required',
             'description' => 'required',
             'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
         ]);
 
         if(empty($request->file('file'))){

            $from = \DB::table('galery')
            ->select('*')
            ->where('id','=', $id)
            ->get();

            $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Update';
            $log->menu = 'Galery - Update';
            $log->url = url()->full();
            $log->keterangan = 'Update data Galery : Kategori Id : '.$request->kategori_id.'
             - Title : '.$request->title.' - Desc : '.$request->description.' | Sebelumnya : '.$from;
            $log->save();

             $galery = \App\Models\galery::findOrFail($id);
             \DB::table('galery')->where('id',$id)->update([
                'kategori_id' => $request->kategori_id,
                 'title' => $request->title,
                 'description' => $request->description,
                 'status' => $request->status
                 ]);
                
                 return redirect('/admin/galery')->withSuccess('success.');
 
         }else{
 
             if ($validator->fails()) {
                 return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan pdf atau file ukuran melebihi 10000']);  
             }
 
             $file = $request->file('file');
         
             $filenameWithExt = $request->file('file')->getClientOriginalName();
             $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
             $extension = $request->file('file')->getClientOriginalExtension();
             $filenameSimpan = $filename.'_'.time().'.'.$extension;
             $path = $request->file('file')->storeAs('/public/galery', $filenameSimpan);
 
             $from = \DB::table('galery')
            ->select('*')
            ->where('id','=', $id)
            ->get();

            $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Update';
            $log->menu = 'Galery - Update';
            $log->url = url()->full();
            $log->keterangan = 'Update data Galery : Kategori Id : '.$request->kategori_id.'
             - Title : '.$request->title.' - Desc : '.$request->description.' - File : '.$filenameSimpan.'
              | Sebelumnya : '.$from;
            $log->save();

             $galery = \App\Models\galery::findOrFail($id);
             \DB::table('galery')->where('id',$id)->update([
                'kategori_id' => $request->kategori_id,
                 'title' => $request->title,
                 'description' => $request->description,
                 'status' => $request->status,
                 'file' => $filenameSimpan
                 ]);
                
                 return redirect('/admin/galery')->withSuccess('success.');
 
         }
 
     }
     //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('galery')
        ->select('id','kategori_id','title', 'description', 'status','file','created_at')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.galeri.edit',['role'=>$role]);
    }    
}

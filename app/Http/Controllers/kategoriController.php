<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\kategori;

class kategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('kategori')
            ->select('id','name','status','created_at')
            ->paginate(10);
			
		return view('admin.kategori.index',['role'=>$role]);
    }    

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('kategori')
        ->select('id','name', 'status')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.kategori.edit',['role'=>$role]);
    }    
    
    public function add()
    {
        return view('admin.kategori.add');
    }

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('kategori')
            ->select('id','name', 'status','created_at')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.kategori.show',['role'=>$role]);
    }    

    public function destroy($id)
    {

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'Kategori - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data Kategori';
      $log->save();

    $kategori = \App\Models\kategori::findOrFail($id);
    $kategori->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    //Save kategori
    public function create(Request $request)
    {
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'Kategori - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data Kategori';
      $log->save();

           $kategori = new \App\Models\kategori;
           $kategori->name = $request->name;
           $kategori->status = '1';
           $kategori->save();
   
           
           return redirect('/admin/kategori')->with('sukses','Data Berhasil Buat');
    }

    //update kategori
    public function update(Request $request, $id)
    {

      $from = \DB::table('kategori')
            ->select('*')
            ->where('id','=', $id)
            ->get();

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Update';
      $log->menu = 'Kategori - Update';
      $log->url = url()->full();
      $log->keterangan = 'Update data Kategori : nama : '.$request->name.'
       - Status : '.$request->status.' - Sebelumnya : '.$from;
      $log->save();

        $kategori = \App\Models\kategori::findOrFail($id);
             \DB::table('kategori')->where('id',$id)->update([
                 'name' => $request->name,
                 'status' => $request->status
                 ]);
                
                 return redirect('/admin/kategori')->withSuccess('success.');
    }

}
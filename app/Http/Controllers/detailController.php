<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\reg_mubes;
use Illuminate\Support\Facades\Mail;
use App\Mail\iaitiMail;

class detailController extends Controller
{
    //show
    public function banner(Request $request, $id)
    {
        $open=Crypt::decryptString($id);
		$role = \DB::table('banner')
            ->select('id','title', 'description', 'link', 'status','file','created_at')
            ->where('id', '=', $open)
            ->where('status', '=', 1)
            ->get();
			
		return view('detail.banner',['role'=>$role]);
    }    

     //show
     public function vs()
     {
         $role = \DB::table('about')
             ->select('*')
             ->get();
             
         return view('detail.visi-misi',['role'=>$role]);
     }    

     //show
     public function so()
     {
         $role = \DB::table('about')
             ->select('*')
             ->get();
             
         return view('detail.so',['role'=>$role]);
     } 
     //show
     public function galery(Request $request, $id)
     {
        $open=Crypt::decryptString($id);
         $role = \DB::table('galery')
             ->select('*')
             ->where('id', '=', $open)
             ->where('status', '=', 1)
             ->get();
             
         return view('detail.galery',['role'=>$role]);
     }    

     //show
     public function tracer()
     {
         $role = \DB::table('tracer')
             ->select('*')
             ->where('status', '=', 1)
             ->get();
             
         return view('detail.tracer',['role'=>$role]);
     }    

     //show
     public function kontak()
     {
         $role = \DB::table('kontak')
             ->select('*')
             ->get();
             
         return view('detail.kontak',['role'=>$role]);
     }    

     //show
     public function mubes()
     {
         $role = \DB::table('mubes')
             ->select('*')
             ->get();
             
         return view('detail.mubes',['role'=>$role]);
     }   
     
     //DPT
     public function DPT()
     {
         $role = \DB::table('mubes')
             ->select('*')
             ->get();
             
         return view('detail.dpt',['role'=>$role]);
     }   
     
     //show
     public function register()
     {
         $role = \DB::table('mubes')
             ->select('*')
             ->get();
             
         return view('detail.register',['role'=>$role]);
     }    

     //Save register
    public function create(Request $request)
    {
        if(empty($request->nim)){
            $nim = uniqid(); 
        }else{
            $nim = $request->nim;
        }
        function alphanumeric($panjang)
        {
                $data = array(
    
                    range(0, 9),
                    range('a', 'z'),
                    range('A','Z')
    
                );
    
                $karakter = array();
                foreach ($data as $key => $value) {
                    foreach ($value as $k => $v) {
                        $karakter[]=$v;
                    }
                }
                $data = null;
    
                for($i=0; $i<=$panjang; $i++)
                {
                    $data .=$karakter[rand($i, count($karakter)-1)];
    
                }
                return $data;
        }
            
        /* #. Bagian Cek Data di data Users Table */  
        $users = \DB::table('users')->where('email', '=', $request->email)->get();
        $users = $users->count();

        if ($users > 0) {
            return redirect()->back()->withErrors(['Error', 'Anda Sudah Pernah melakukan Registrasi sebelumnya, silahkan login atau hub. Admin']);
        }else{


        /* 1. Bagian Cek Data di data alumni Table */  
        $iaiti = \DB::table('alumni_iti')->where('NIM', '=', $nim)->get();
        $iaiti = $iaiti->count();
        $ia = \DB::table('alumni_84_99')->where('NIM', '=', $nim)->get();
        $ia = $ia->count();

        if ($iaiti > 0) {
              /* 2. Bagian Cek Data di Register Table */  
            $reg = \DB::table('reg_mubes')->where('nim', '=', $nim)->get();
            $reg = $reg->count();
            if ($reg > 0) {
                return redirect()->back()->withErrors(['Error', 'Anda Sudah Pernah melakukan Registrasi sebelumnya, silahkan login atau hub. Admin']);
            }else{

                $validator = Validator::make($request->all(), [
                    'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048',
                ]);
        
                   if ($validator->fails()) {
                       return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan Images(jpg,png,jpeg,gif,svg) atau file ukuran melebihi 3 Mb']);  
                   }
        
                   $listuser = new \App\Models\listuser;
                   $listuser->name = $request->username;
                   $listuser->email = $request->email;
                   $listuser->password = bcrypt('123456');
                   $listuser->remember_token = bcrypt('1234');
                   $listuser->save();
        
                   $roles = new \App\Models\user_roles;
                   $roles->id_user = $listuser->id;
                   $roles->id_role = '4';
                   $roles->save();
        
                   $file = $request->file('file');
                   
                   $filenameWithExt = $request->file('file')->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                   $extension = $request->file('file')->getClientOriginalExtension();
                   $filenameSimpan = $filename.'_'.time().'.'.$extension;
                   $path = $request->file('file')->storeAs('/public/profil', $filenameSimpan);
           
                   $reg = new \App\Models\reg_mubes;
                   $reg->id_user = $listuser->id;
                   $reg->nim = $nim;
                   $reg->name = $request->name;
                   $reg->prodi = $request->prodi;
                   $reg->extra_a = $request->extra_a;
                   $reg->extra_b = $request->extra_b;
                   $reg->phone = $request->phone1.''.$request->phone2.''.$request->phone3;
                   $reg->angkatan = $request->angkatan;
                   $reg->kode = alphanumeric(6);
                   $reg->status = '1';
                   $reg->file = $filenameSimpan;
                   $reg->save();

                   $details = [
                    'title' => 'Register Success',
                    'body' => 'Status Anda Telah Approved by System',
                    'username' => $request->username,
                    'nim' => $nim,
                    'kode' => $reg->kode
                     ];
        
                    Mail::to($request->email)->send(new iaitiMail($details));
                   
                   return redirect()->back()->with('success', 'Data anda berhasil disimpan, silahkan cek email anda untuk melanjutkan proses selanjutnya'); 

            }
            /* 2.(TUTUP) Bagian Cek Data di Register Table */  

            }else if($ia > 0) {

                /* 2. Bagian Cek Data di Register Table */  
            $reg = \DB::table('reg_mubes')->where('nim', '=', $nim)->get();
            $reg = $reg->count();
            if ($reg > 0) {
                return redirect()->back()->withErrors(['Error', 'Anda Sudah Pernah melakukan Registrasi sebelumnya, silahkan login atau hub. Admin']);
            }else{

                $validator = Validator::make($request->all(), [
                    'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048',
                ]);
        
                   if ($validator->fails()) {
                       return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan Images(jpg,png,jpeg,gif,svg) atau file ukuran melebihi 3 Mb']);  
                   }
        
                   $listuser = new \App\Models\listuser;
                   $listuser->name = $request->username;
                   $listuser->email = $request->email;
                   $listuser->password = bcrypt('123456');
                   $listuser->remember_token = bcrypt('1234');
                   $listuser->save();
        
                   $roles = new \App\Models\user_roles;
                   $roles->id_user = $listuser->id;
                   $roles->id_role = '4';
                   $roles->save();
        
                   $file = $request->file('file');
                   
                   $filenameWithExt = $request->file('file')->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                   $extension = $request->file('file')->getClientOriginalExtension();
                   $filenameSimpan = $filename.'_'.time().'.'.$extension;
                   $path = $request->file('file')->storeAs('/public/profil', $filenameSimpan);
           
                   $reg = new \App\Models\reg_mubes;
                   $reg->id_user = $listuser->id;
                   $reg->nim = $nim;
                   $reg->name = $request->name;
                   $reg->prodi = $request->prodi;
                   $reg->extra_a = $request->extra_a;
                   $reg->extra_b = $request->extra_b;
                   $reg->phone = $request->phone1.''.$request->phone2.''.$request->phone3;
                   $reg->angkatan = $request->angkatan;
                   $reg->kode = alphanumeric(6);
                   $reg->status = '1';
                   $reg->file = $filenameSimpan;
                   $reg->save();

                   $details = [
                    'title' => 'Register Success',
                    'body' => 'Status Anda Telah Approved by System',
                    'username' => $request->username,
                    'nim' => $nim,
                    'kode' => $reg->kode
                     ];
        
                    Mail::to($request->email)->send(new iaitiMail($details));
                   
                   return redirect()->back()->with('success', 'Data anda berhasil disimpan, silahkan cek email anda untuk melanjutkan proses selanjutnya'); 

            }
            /* 2.(TUTUP) Bagian Cek Data di Register Table */  

            }else{

                /* 2. Bagian Cek Data di Register Table */  
            $reg2 = \DB::table('reg_mubes')->where('nim', '=', $nim)->get();
            $reg2 = $reg2->count();
            if ($reg2 > 0) {

                return redirect()->back()->with('success', 'Anda Sudah Pernah melakukan Registrasi sebelumnya, silahkan login atau hub. Admin');
            }else{

                $validator = Validator::make($request->all(), [
                    'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048',
                ]);
        
                   if ($validator->fails()) {
                       return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan Images(jpg,png,jpeg,gif,svg) atau file ukuran melebihi 3Mb']);  
                   }
        
                   $listuser = new \App\Models\listuser;
                   $listuser->name = $request->username;
                   $listuser->email = $request->email;
                   $listuser->password = bcrypt('123456');
                   $listuser->remember_token = bcrypt('1234');
                   $listuser->save();
        
                   $roles = new \App\Models\user_roles;
                   $roles->id_user = $listuser->id;
                   $roles->id_role = '4';
                   $roles->save();
        
                   $file = $request->file('file');
                   
                   $filenameWithExt = $request->file('file')->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                   $extension = $request->file('file')->getClientOriginalExtension();
                   $filenameSimpan = $filename.'_'.time().'.'.$extension;
                   $path = $request->file('file')->storeAs('/public/profil', $filenameSimpan);
           
                   $reg = new \App\Models\reg_mubes;
                   $reg->id_user = $listuser->id;
                   $reg->nim = $nim;
                   $reg->name = $request->name;
                   $reg->prodi = $request->prodi;
                   $reg->extra_a = $request->extra_a;
                   $reg->extra_b = $request->extra_b;
                   $reg->phone = $request->phone1.''.$request->phone2.''.$request->phone3;
                   $reg->angkatan = $request->angkatan;
                   $reg->kode = alphanumeric(6);
                   $reg->status = '0';
                   $reg->file = $filenameSimpan;
                   $reg->save();

                   $details = [
                    'title' => 'Register Success',
                    'body' => 'Status Anda Masih Menunggu Di Approve, silahkan menunggu informasi selanjutnya',
                    'username' => $request->username,
                    'nim' => $request->nim,
                    'kode' => 'Waitting'
                     ];
        
                    Mail::to($request->email)->send(new iaitiMail($details));
                   
                   return redirect()->back()->with('success', 'Data anda berhasil disimpan, silahkan cek email anda untuk melanjutkan proses selanjutnya'); 

            }
            /* 2.(TUTUP) Bagian Cek Data di Register Table */  
            }
            /* 1.(TUTUP) Bagian Cek Data di data alumni Table */   

        /*
        
        */
        }
    }

}
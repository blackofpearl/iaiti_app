<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class prodiController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('prodi')
            ->select('*')
            ->paginate(10);
			
		return view('admin.prodi.index',['role'=>$role]);
    }    

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('prodi')
            ->select('*')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.prodi.edit',['role'=>$role]);
    }    
    
    public function add()
    {
        return view('admin.prodi.add');
    }

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('prodi')
            ->select('*')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.prodi.show',['role'=>$role]);
    }    

    public function destroy($id)
    {
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'Prodi - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data Prodi';
      $log->save();

    $prodi = \App\Models\prodi::findOrFail($id);
    $prodi->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    //Save prodi
    public function create(Request $request)
    {
             
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'Prodi - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data Prodi';
      $log->save();

           $prodi = new \App\Models\prodi;
           $prodi->code = $request->code;
           $prodi->name = $request->name;
           $prodi->status = $request->status;
           $prodi->save();
   
           
           return redirect('/admin/prodi')->with('sukses','Data Berhasil Buat');
    }

    //update prodi
    public function update(Request $request, $id)
    {

      $from = \DB::table('prodi')
        ->select('*')
        ->where('id','=', $id)
        ->get();

  $ip=\request()->ip();
  date_default_timezone_set("Asia/Jakarta");
  $log = new \App\Models\log_activity;
  $log->id_user = \Auth::user()->id;
  $log->name = \Auth::user()->name;
  $log->email = \Auth::user()->email;
  $log->date_log = date('Y-m-d H:i:s');
  $log->ip = $ip;
  $log->status = 'Update';
  $log->menu = 'Mubes - Update';
  $log->url = url()->full();
  $log->keterangan = 'Update data Mubes : Code : '.$request->code.'
   - Name : '.$request->name.' - Sebelumnya : '.$from;
  $log->save();

        $prodi = \App\Models\prodi::findOrFail($id);
             \DB::table('prodi')->where('id',$id)->update([
                'code' => $request->code,
                'name' => $request->name,
                'status' => $request->status
                 ]);
                 return redirect('/admin/prodi')->with('sukses','Data Berhasil Buat');
}
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\iaitiMail;

class mailController extends Controller
{
    public function sendMail()
    {
        $details = [
            'title' => 'Mail from test',
            'body' => 'testing cuy'
        ];

        Mail::to("admiaiti@gmail.com")->send(new iaitiMail($details));
        return "email sent";
    }
}

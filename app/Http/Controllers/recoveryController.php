<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\listuser;
use Illuminate\Support\Facades\Mail;
use App\Mail\iaitiMail;

class recoveryController extends Controller
{
    public function recovery(Request $request)
    {
		$cek1 = \DB::table('users')
                ->join('reg_mubes','reg_mubes.id_user','=','users.id')
                ->where('users.email', '=', $request->email)
                ->where('reg_mubes.status','<','1')
                ->get();
        $cek1 = $cek1->count();
        
        if($cek1 > 0){

            return redirect()->back()->withErrors(['Info', 'Status Anda waiting, silahkan tunggu atau hubungi admin']);

        }else{
			
        function alphanumeric($panjang)
        {
                $data = array(
      
                    range(0, 9),
                    range('a', 'z'),
                    range('A','Z')
      
                );
      
                $karakter = array();
                foreach ($data as $key => $value) {
                    foreach ($value as $k => $v) {
                        $karakter[]=$v;
                    }
                }
                $data = null;
      
                for($i=0; $i<=$panjang; $i++)
                {
                    $data .=$karakter[rand($i, count($karakter)-1)];
      
                }
                return $data;
        } 
        $kode=alphanumeric(6);
    $hit = \DB::table('users')->where('email', '=', $request->email)->get();
    $hiat = $hit->count();
        
        if($hiat > 0){
        
        $role = \DB::table('users')
        ->join('reg_mubes','reg_mubes.id_user','=','users.id')
        ->join('user_roles','users.id','=','user_roles.id_user')
        ->join('roles','roles.id','=','user_roles.id_role')
        ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                  , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                  , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
        ->where('users.email', '=', $request->email)
        ->get();

        foreach($role as $na)
        {
            $reg = new \App\Models\reg_mubes;
            \DB::table('reg_mubes')->where('id_user',$na->id)->update([
               'kode' => $kode
                ]);

            $details = [
                'title' => 'Register Success',
                'body' => 'Recovery kode sudah dalam posisi terupdate',
                'username' => $na->name,
                'nim' => $na->nim,
                'kode' => $kode
                 ];
        
                Mail::to($request->email)->send(new iaitiMail($details));
        
        return redirect('/Register-Mubes')->with('success', 'Code Sudah Di Update, Silahkan Cek Email anda');
        }

        }else{
        
            return redirect()->back()->withErrors(['Error', 'Email Anda Tidak Ditemukan, Silahkan hubungi admin']);  
        
        }

        }
	}
}

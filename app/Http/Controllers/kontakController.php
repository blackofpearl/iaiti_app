<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class kontakController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('kontak')
            ->select('id','title','address','description','gmaps','email','phone','fax','copyright','design','fb','twitter','instagram','linkedin')
            ->paginate(10);
			
		return view('admin.kontak.index',['role'=>$role]);
    }    

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('kontak')
            ->select('id','title','address','description','gmaps','email','phone','fax','copyright','design','fb','twitter','instagram','linkedin')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.kontak.edit',['role'=>$role]);
    }    
    
    public function add()
    {
        return view('admin.kontak.add');
    }

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('kontak')
            ->select('id','title','address','description','gmaps','email','phone','fax','copyright','design','fb','twitter','instagram','linkedin')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.kontak.show',['role'=>$role]);
    }    

    public function destroy($id)
    {
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'Kontak - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data Kontak';
      $log->save();

    $kontak = \App\Models\kontak::findOrFail($id);
    $kontak->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    //Save kontak
    public function create(Request $request)
    {

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'Kontak - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data Kontak';
      $log->save();
             
           $kontak = new \App\Models\kontak;
           $kontak->title = $request->title;
           $kontak->address = $request->address;
           $kontak->description = $request->description;
           $kontak->gmaps = $request->gmaps;
           $kontak->email = $request->email;
           $kontak->phone = $request->phone;
           $kontak->fax = $request->fax;
           $kontak->copyright = $request->copyright;
           $kontak->design = $request->design;
           $kontak->fb = $request->fb;
           $kontak->twitter = $request->twitter;
           $kontak->instagram = $request->instagram;
           $kontak->linkedin = $request->linkedin;
           $kontak->save();
   
           
           return redirect('/admin/kontak')->with('sukses','Data Berhasil Buat');
    }

    //update kontak
    public function update(Request $request, $id)
    {

      $from = \DB::table('kontak')
            ->select('*')
            ->where('id','=', $id)
            ->get();

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Update';
      $log->menu = 'Kontak - Update';
      $log->url = url()->full();
      $log->keterangan = 'Update data Kontak title : '.$request->title.' | address : '.$request->address.'
      | Description : '.$request->description.' | gmaps : '.$request->gmaps.' | Sebelumnya :'.$from;
      $log->save();

        $kontak = \App\Models\kontak::findOrFail($id);
             \DB::table('kontak')->where('id',$id)->update([
                'title' => $request->title,
                'address' => $request->address,
                'description' => $request->description,
                'gmaps' => $request->gmaps,
                'email' => $request->email,
                'phone' => $request->phone,
                'fax' => $request->fax,
                'copyright' => $request->copyright,
                'design' => $request->design,
                'fb' => $request->fb,
                'twitter' => $request->twitter,
                'instagram' => $request->instagram,
                'linkedin' => $request->linkedin
                 ]);
                
                 return redirect('/admin/kontak')->withSuccess('success.');
    }
}

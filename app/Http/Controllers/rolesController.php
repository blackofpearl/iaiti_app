<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class rolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('roles')
            ->select('*')
            ->paginate(10);
			
		return view('admin.roles.index',['role'=>$role]);
    }    

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('roles')
            ->select('*')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.roles.edit',['role'=>$role]);
    }    
    
    public function add()
    {
        return view('admin.roles.add');
    }

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('roles')
            ->select('*')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.roles.show',['role'=>$role]);
    }    

    public function destroy($id)
    {

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'Roles - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data Roles';
      $log->save();

    $roles = \App\Models\roles::findOrFail($id);
    $roles->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    //Save roles
    public function create(Request $request)
    {
             
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'Roles - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data Roles';
      $log->save();

           $roles = new \App\Models\roles;
           $roles->role_user = $request->role_user;
           $roles->status = $request->status;
           $roles->save();
   
           
           return redirect('/admin/roles')->with('sukses','Data Berhasil Buat');
    }

    //update roles
    public function update(Request $request, $id)
    {
      $from = \DB::table('roles')
        ->select('*')
        ->where('id','=', $id)
        ->get();

  $ip=\request()->ip();
  date_default_timezone_set("Asia/Jakarta");
  $log = new \App\Models\log_activity;
  $log->id_user = \Auth::user()->id;
  $log->name = \Auth::user()->name;
  $log->email = \Auth::user()->email;
  $log->date_log = date('Y-m-d H:i:s');
  $log->ip = $ip;
  $log->status = 'Update';
  $log->menu = 'Roles - Update';
  $log->url = url()->full();
  $log->keterangan = 'Update data Roles : role Users : '.$request->role_user.'
   - status : '.$request->status.' - Sebelumnya : '.$from;
  $log->save();

        $roles = \App\Models\roles::findOrFail($id);
             \DB::table('roles')->where('id',$id)->update([
                'role_user' => $request->role_user,
                'status' => $request->status
                 ]);
                
                 return redirect('/admin/roles')->withSuccess('success.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\reg_mubes;
use Illuminate\Support\Facades\Mail;
use App\Mail\iaitiMail;

class approvedController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $role = \DB::table('users')
        ->join('reg_mubes','reg_mubes.id_user','=','users.id')
        ->join('user_roles','users.id','=','user_roles.id_user')
        ->join('roles','roles.id','=','user_roles.id_role')
        ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                        , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                        , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
              ->where('roles.id', '>', '3')
              ->Where('reg_mubes.status', '=', '0')
              ->OrWhere('reg_mubes.status', '=', '3')
              ->orderBy('reg_mubes.id','DESC')
              ->paginate(6);
			
		return view('admin.approved.index',['role'=>$role]);
    }    

    public function cari(Request $request)
      {
        // menangkap data pencarian
        $cari = $request->cari;
    
            // mengambil data dari table pegawai sesuai pencarian data
                $role = \DB::table('users')
                ->join('reg_mubes','reg_mubes.id_user','=','users.id')
                ->join('user_roles','users.id','=','user_roles.id_user')
                ->join('roles','roles.id','=','user_roles.id_role')
                ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                                , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                                , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
            ->Where('reg_mubes.status', '<>', '1')
            ->Where('reg_mubes.status', '<>', '2')
        ->Where('reg_mubes.name','like',"%".$cari."%")
        ->paginate();
    
            // mengirim data pegawai ke view index
        return view('admin.approved.index',['role'=>$role]);
    
      }

      public function cari2(Request $request)
      {
        // menangkap data pencarian
        $cari2 = $request->cari2;
    
            // mengambil data dari table pegawai sesuai pencarian data
                $role = \DB::table('users')
                ->join('reg_mubes','reg_mubes.id_user','=','users.id')
                ->join('user_roles','users.id','=','user_roles.id_user')
                ->join('roles','roles.id','=','user_roles.id_role')
                ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                                , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                                , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
            ->Where('reg_mubes.status', '<>', '1')
            ->Where('reg_mubes.status', '<>', '2')
        ->Where('reg_mubes.prodi','=',$cari2)
        ->paginate();
    
            // mengirim data pegawai ke view index
        return view('admin.approved.index',['role'=>$role]);
    
      }

    public function edit(Request $request, $id)
    {
			$role = \DB::table('users')
            ->join('reg_mubes','reg_mubes.id_user','=','users.id')
            ->join('user_roles','users.id','=','user_roles.id_user')
            ->join('roles','roles.id','=','user_roles.id_role')
            ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                      , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                      , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
    ->where('users.id', '=', $id)
    ->get();
			
		return view('admin.approved.edit',['role'=>$role]);
    } 
    
    public function all(Request $request)
    {
        if(empty($request->chk)){
            return redirect()->back()->withErrors(['Error', 'Tidak Ada Checkbox yang diPilih']);
        }else{
            $a=0;
                foreach($request->chk as $on){

                    $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$request->chk[$a])->update([
                        'status' => $request->status
                        ]);

                        if($request->status == 2){
                          $details = [
                            'title' => 'Register Success',
                            'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                            'username' => $request->username[$a],
                            'nim' => $request->nim[$a],
                            'kode' => $request->kode[$a]
                             ];
                
                            Mail::to($request->email[$a])->send(new iaitiMail($details));

                        }else if($request->status == 3){
                          $details = [
                            'title' => 'Register Failed',
                            'body' => 'Status Akun Tidak Di Setujui',
                            'username' => $request->username[$a],
                            'nim' => $request->nim[$a],
                            'kode' => 'Data Anda Di Tolak'
                             ];
                
                            Mail::to($request->email[$a])->send(new iaitiMail($details));

                        }else{
                          $details = [
                            'title' => 'Register Waitting',
                            'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                            'username' => $request->username[$a],
                            'nim' => $request->nim[$a],
                            'kode' => 'Mohon Di Tunggu Informasinya'
                             ];
                
                            Mail::to($request->email[$a])->send(new iaitiMail($details));
                        }

                $a++;
                }
                return redirect('/admin/approved')->with('success', 'Data Berhasil Update');
        }
        
    } 
    
    //update listuser
    public function update(Request $request, $id)
    {
      
      //cek images kosong
      if(empty($request->file)){
       
        //password kosong
        if(empty($request->password)){
          $listuser = \App\Models\listuser::findOrFail($id);
               \DB::table('users')->where('id',$id)->update([
                  'name' => $request->username,
                  'email' => $request->email
                   ]);

          $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$id)->update([
                          'nim' => $request->nim,
                          'name' => $request->name,
                          'prodi' => $request->prodi,
                          'phone' => $request->phone1.''.$request->phone2.''.$request->phone3,
                          'status' => $request->status,
                          'extra_a' => $request->extra_a,
                          'extra_b' => $request->extra_b,
                          'angkatan' => $request->angkatan
                           ]);

                           if($request->status == 2){
                            $ip=\request()->ip();
                            date_default_timezone_set("Asia/Jakarta");
                            $log = new \App\Models\log_activity;
                            $log->id_user = \Auth::user()->id;
                            $log->name = \Auth::user()->name;
                            $log->email = \Auth::user()->email;
                            $log->date_log = date('Y-m-d H:i:s');
                            $log->ip = $ip;
                            $log->status = 'Approval';
                            $log->menu = 'Approved Users - approve';
                            $log->url = url()->full();
                            $log->keterangan = 'Approve user : '.$request->name.' | Status : Disetujui';
                            $log->save();

                            $details = [
                              'title' => 'Register Success',
                              'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => $request->kode
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else if($request->status == 3){
                            $ip=\request()->ip();
                            date_default_timezone_set("Asia/Jakarta");
                            $log = new \App\Models\log_activity;
                            $log->id_user = \Auth::user()->id;
                            $log->name = \Auth::user()->name;
                            $log->email = \Auth::user()->email;
                            $log->date_log = date('Y-m-d H:i:s');
                            $log->ip = $ip;
                            $log->status = 'Approval';
                            $log->menu = 'Approved Users - Not approve';
                            $log->url = url()->full();
                            $log->keterangan = 'Approve user : '.$request->name.' | Status : Tidak Disetujui';
                            $log->save();

                            $details = [
                              'title' => 'Register Failed',
                              'body' => 'Status Akun Tidak Di Setujui',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Data Anda Di Tolak'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else{
                            $ip=\request()->ip();
                            date_default_timezone_set("Asia/Jakarta");
                            $log = new \App\Models\log_activity;
                            $log->id_user = \Auth::user()->id;
                            $log->name = \Auth::user()->name;
                            $log->email = \Auth::user()->email;
                            $log->date_log = date('Y-m-d H:i:s');
                            $log->ip = $ip;
                            $log->status = 'Approval';
                            $log->menu = 'Approved Users - approve Waitting';
                            $log->url = url()->full();
                            $log->keterangan = 'Approve user : '.$request->name.' | Status : Menunggu';
                            $log->save();

                            $details = [
                              'title' => 'Register Waitting',
                              'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Mohon Di Tunggu Informasinya'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
                          }
                       
          return redirect('/admin/approved')->with('success', 'Data Berhasil Update');

         //password isi 
        }else{
          
        $pass=$request->password;
        $uppercase = preg_match('@[A-Z]@', $pass);
        $lowercase = preg_match('@[a-z]@', $pass);
        $number    = preg_match('@[0-9]@', $pass);
  
        if(!$uppercase || !$lowercase || !$number || strlen($pass) <=6 ){
  
          return redirect()->back()->withErrors(['Error', 'Password Wajib minimum 8 Character dan mengandung huruf BESAR, huruf kecil dan angka']);
  
        }else{
  
          $listuser = \App\Models\listuser::findOrFail($id);
               \DB::table('users')->where('id',$id)->update([
                  'name' => $request->username,
                  'email' => $request->email,
                  'password' => md5($request->password)
                   ]);

                       $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$id)->update([
                          'nim' => $request->nim,
                          'name' => $request->name,
                          'prodi' => $request->prodi,
                          'phone' => $request->phone1.''.$request->phone2.''.$request->phone3,
                          'status' => $request->status,
                          'extra_a' => $request->extra_a,
                          'extra_b' => $request->extra_b,
                          'angkatan' => $request->angkatan
                           ]);

                           if($request->status == 2){
                            $details = [
                              'title' => 'Register Success',
                              'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => $request->kode
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else if($request->status == 3){
                            $details = [
                              'title' => 'Register Failed',
                              'body' => 'Status Akun Tidak Di Setujui',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Data Anda Di Tolak'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else{
                            $details = [
                              'title' => 'Register Waitting',
                              'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Mohon Di Tunggu Informasinya'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
                          }
  
          return redirect('/admin/approved')->with('success', 'Data Berhasil Update'); 
        }
  
        }

      //jika images ada
      }else{

        $validator = Validator::make($request->all(), [
          'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048',
      ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan Images(jpg,png,jpeg,gif,svg) atau file ukuran melebihi 10000']);  
         }

        //password kosong
        if(empty($request->password)){
          $listuser = \App\Models\listuser::findOrFail($id);
               \DB::table('users')->where('id',$id)->update([
                  'name' => $request->username,
                  'email' => $request->email
                   ]);
         

			$file = $request->file('file');
                   
                   $filenameWithExt = $request->file('file')->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                   $extension = $request->file('file')->getClientOriginalExtension();
                   $filenameSimpan = $filename.'_'.time().'.'.$extension;
                   $path = $request->file('file')->storeAs('/public/profil', $filenameSimpan);
			
          $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$id)->update([
                          'nim' => $request->nim,
                          'name' => $request->name,
                          'prodi' => $request->prodi,
                          'phone' => $request->phone1.''.$request->phone2.''.$request->phone3,
                          'status' => $request->status,
                          'extra_a' => $request->extra_a,
                          'extra_b' => $request->extra_b,
                          'angkatan' => $request->angkatan,
                          'file' => $filenameSimpan
                           ]);

                           if($request->status == 2){
                            $details = [
                              'title' => 'Register Success',
                              'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => $request->kode
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else if($request->status == 3){
                            $details = [
                              'title' => 'Register Failed',
                              'body' => 'Status Akun Tidak Di Setujui',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Data Anda Di Tolak'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else{
                            $details = [
                              'title' => 'Register Waitting',
                              'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Mohon Di Tunggu Informasinya'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
                          }
                       
          return redirect('/admin/approved')->with('success', 'Data Berhasil Update');

         //password isi 
        }else{
          
          $pass=$request->password;
        $uppercase = preg_match('@[A-Z]@', $pass);
        $lowercase = preg_match('@[a-z]@', $pass);
        $number    = preg_match('@[0-9]@', $pass);
  
        if(!$uppercase || !$lowercase || !$number || strlen($pass) <=6 ){
  
          return redirect()->back()->withErrors(['Error', 'Password Wajib minimum 8 Character dan mengandung huruf BESAR, huruf kecil dan angka']);
  
        }else{
  
			$listuser = \App\Models\listuser::findOrFail($id);
               \DB::table('users')->where('id',$id)->update([
                  'name' => $request->username,
                  'email' => $request->email,
                  'password' => md5($request->password)
                   ]);

			$file = $request->file('file');
                   
                   $filenameWithExt = $request->file('file')->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                   $extension = $request->file('file')->getClientOriginalExtension();
                   $filenameSimpan = $filename.'_'.time().'.'.$extension;
                   $path = $request->file('file')->storeAs('/public/profil', $filenameSimpan);
			
          $reg = new \App\Models\reg_mubes;
                       \DB::table('reg_mubes')->where('id_user',$id)->update([
                          'nim' => $request->nim,
                          'name' => $request->name,
                          'prodi' => $request->prodi,
                          'phone' => $request->phone1.''.$request->phone2.''.$request->phone3,
                          'status' => $request->status,
                          'extra_a' => $request->extra_a,
                          'extra_b' => $request->extra_b,
                          'angkatan' => $request->angkatan,
                          'file' => $filenameSimpan
                           ]);

                           if($request->status == 2){
                            $details = [
                              'title' => 'Register Success',
                              'body' => 'Anda Di Setujui, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => $request->kode
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else if($request->status == 3){
                            $details = [
                              'title' => 'Register Failed',
                              'body' => 'Status Akun Tidak Di Setujui',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Data Anda Di Tolak'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
  
                          }else{
                            $details = [
                              'title' => 'Register Waitting',
                              'body' => 'Akun Telah Terdaftar, Silahkan Tunggu Informasi Selanjutnya',
                              'username' => $request->username,
                              'nim' => $request->nim,
                              'kode' => 'Mohon Di Tunggu Informasinya'
                               ];
                  
                              Mail::to($request->email)->send(new iaitiMail($details));
                          }
  
          return redirect('/admin/approved')->with('success', 'Data Berhasil Update'); 
        }

        }
        
      }

    }

        public function destroy($id)
        {
                            $from = \DB::table('users')
                            ->select('id','name','email')
                            ->get();
                            $ip=\request()->ip();
                            date_default_timezone_set("Asia/Jakarta");
                            $log = new \App\Models\log_activity;
                            $log->id_user = \Auth::user()->id;
                            $log->name = \Auth::user()->name;
                            $log->email = \Auth::user()->email;
                            $log->date_log = date('Y-m-d H:i:s');
                            $log->ip = $ip;
                            $log->status = 'Delete';
                            $log->menu = 'Delete Users Join';
                            $log->url = url()->full();
                            $log->keterangan = 'Delete user roles, reg_mubes, users : '.$form;
                            $log->save();

            \DB::delete('delete from user_roles where id_user = ?', [$id]);
    
            \DB::delete('delete from reg_mubes where id_user = ?', [$id]);
    
            \DB::delete('delete from users where id = ?', [$id]);

        return redirect('/admin/approved')->with('success', 'Data Berhasil Dihapus'); 
        }
}

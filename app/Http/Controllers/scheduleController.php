<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class scheduleController extends Controller
{
         /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index()
    {
		$role = \DB::table('schedule')
            ->select('*')
            ->paginate(10);
			
		return view('admin.schedule.index',['role'=>$role]);
    }    

    public function add()
    {
        return view('admin.schedule.add');
    }

    //Save schedule
    public function create(Request $request)
    {
             
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'Schedule - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data Schedule';
      $log->save();

           $schedule = new \App\Models\schedule;
           $schedule->title = $request->title;
           $schedule->periode_a = $request->periode_a;
           $schedule->periode_b = $request->periode_b;
           $schedule->extra_a = $request->extra_a;
           $schedule->status = $request->status;
           $schedule->save(); 
   
           
           return redirect('/admin/schedule')->with('sukses','Data Berhasil Buat');
    }

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('schedule')
            ->select('*')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.schedule.edit',['role'=>$role]);
    }    

    public function destroy($id)
    {

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'Schedule - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data Schedule';
      $log->save();

    $schedule = \App\Models\schedule::findOrFail($id);
    $schedule->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    //update schedule
    public function update(Request $request, $id)
    {

      $from = \DB::table('schedule')
      ->select('*')
      ->where('id','=', $id)
      ->get();

$ip=\request()->ip();
date_default_timezone_set("Asia/Jakarta");
$log = new \App\Models\log_activity;
$log->id_user = \Auth::user()->id;
$log->name = \Auth::user()->name;
$log->email = \Auth::user()->email;
$log->date_log = date('Y-m-d H:i:s');
$log->ip = $ip;
$log->status = 'Update';
$log->menu = 'Schedule - Update';
$log->url = url()->full();
$log->keterangan = 'Update data Schedule : Title : '.$request->title.'
 - Periode mulai : '.$request->periode_a.'- Periode akhir : '.$request->periode_b.'
  - Sebelumnya : '.$from;
$log->save();

        $schedule = \App\Models\schedule::findOrFail($id);
             \DB::table('schedule')->where('id',$id)->update([
                'title' => $request->title,
                'periode_a' => $request->periode_a,
                'periode_b' => $request->periode_b,
                'extra_a' => $request->extra_a,
                'status' => $request->status
                 ]);
                
                 return redirect('/admin/schedule')->withSuccess('success.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\majalah;

class majalahController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('majalah')
            ->select('id','title', 'description', 'link', 'status','file','created_at')
            ->paginate(2);
			
		return view('admin.majalah.index',['role'=>$role]);
    }    

    //Add majalah
    public function add()
    {
        return view('admin.majalah.add');
    }

    //Save majalah
    public function create(Request $request)
    {
        $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'Majalah - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data Majalah';
      $log->save();
        
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'file' => 'required|mimes:pdf|max:10000',
        ]);

        if(empty($request->file('file'))){
           
           $majalah = new \App\Models\majalah;
           $majalah->title = $request->title;
           $majalah->description = $request->description;
           $majalah->link = $request->link;
           $majalah->status = $request->status;
           $majalah->save();
   
           
           return redirect('/admin/majalah')->with('sukses','Data Berhasil Buat');
        }else{
           if ($validator->fails()) {
               return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan pdf atau file ukuran melebihi 10000']);  
           }
           
           $file = $request->file('file');
           
           $filenameWithExt = $request->file('file')->getClientOriginalName();
           $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
           $extension = $request->file('file')->getClientOriginalExtension();
           $filenameSimpan = $filename.'_'.time().'.'.$extension;
           $path = $request->file('file')->storeAs('/public/majalah', $filenameSimpan);
   
           $majalah = new \App\Models\majalah;
           $majalah->title = $request->title;
           $majalah->description = $request->description;
           $majalah->link = $request->link;
           $majalah->status = $request->status;
           $majalah->file = $filenameSimpan;
           $majalah->save();
   
           
           return redirect('/admin/majalah')->with('sukses','Data Berhasil Buat');
        }

       
    }

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('majalah')
            ->select('id','title', 'description', 'link', 'status','file','created_at')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.majalah.show',['role'=>$role]);
    }    

    public function destroy($id)
    {
        $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'Majalah - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data Majalah';
      $log->save();

    $majalah = \App\Models\majalah::findOrFail($id);
    $majalah->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

     //update majalah
     public function update(Request $request, $id)
     {
        $from = \DB::table('majalah')
        ->select('*')
        ->where('id','=', $id)
        ->get();

  $ip=\request()->ip();
  date_default_timezone_set("Asia/Jakarta");
  $log = new \App\Models\log_activity;
  $log->id_user = \Auth::user()->id;
  $log->name = \Auth::user()->name;
  $log->email = \Auth::user()->email;
  $log->date_log = date('Y-m-d H:i:s');
  $log->ip = $ip;
  $log->status = 'Update';
  $log->menu = 'Majalah - Update';
  $log->url = url()->full();
  $log->keterangan = 'Update data Majalah : Title : '.$request->title.'
   - Description : '.$request->description.' - Sebelumnya : '.$from;
  $log->save();

         $validator = Validator::make($request->all(), [
             'title' => 'required',
             'description' => 'required',
             'file' => 'required|mimes:pdf|max:10000',
         ]);
 
         if(empty($request->file('file'))){
             $majalah = \App\Models\majalah::findOrFail($id);
             \DB::table('majalah')->where('id',$id)->update([
                 'title' => $request->title,
                 'description' => $request->description,
                 'link' => $request->link,
                 'status' => $request->status
                 ]);
                
                 return redirect('/admin/majalah')->withSuccess('success.');
 
         }else{
 
             if ($validator->fails()) {
                 return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan pdf atau file ukuran melebihi 10000']);  
             }
 
             $file = $request->file('file');
         
             $filenameWithExt = $request->file('file')->getClientOriginalName();
             $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
             $extension = $request->file('file')->getClientOriginalExtension();
             $filenameSimpan = $filename.'_'.time().'.'.$extension;
             $path = $request->file('file')->storeAs('/public/majalah', $filenameSimpan);
 
             $majalah = \App\Models\majalah::findOrFail($id);
             \DB::table('majalah')->where('id',$id)->update([
                 'title' => $request->title,
                 'description' => $request->description,
                 'link' => $request->link,
                 'status' => $request->status,
                 'file' => $filenameSimpan
                 ]);
                
                 return redirect('/admin/majalah')->withSuccess('success.');
 
         }
 
     }
     //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('majalah')
        ->select('id','title', 'description', 'link', 'status','file','created_at')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.majalah.edit',['role'=>$role]);
    }    

}

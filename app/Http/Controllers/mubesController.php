<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class mubesController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('mubes')
            ->select('id','title','description')
            ->paginate(10);
			
		return view('admin.mubes.index',['role'=>$role]);
    }    

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('mubes')
            ->select('id','title','description')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.mubes.edit',['role'=>$role]);
    }    
    
    public function add()
    {
        return view('admin.mubes.add');
    }

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('mubes')
            ->select('id','title','description')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.mubes.show',['role'=>$role]);
    }    

    public function destroy($id)
    {

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Delete';
      $log->menu = 'Mubes - Delete';
      $log->url = url()->full();
      $log->keterangan = 'Menghapus data Mubes';
      $log->save();

    $mubes = \App\Models\mubes::findOrFail($id);
    $mubes->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    //Save mubes
    public function create(Request $request)
    {
      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Create';
      $log->menu = 'Mubes - Create';
      $log->url = url()->full();
      $log->keterangan = 'Menambah data Mubes';
      $log->save();

           $mubes = new \App\Models\mubes;
           $mubes->title = $request->title;
           $mubes->description = $request->description;
           $mubes->save();
   
           
           return redirect('/admin/mubes')->with('sukses','Data Berhasil Buat');
    }

    //update mubes
    public function update(Request $request, $id)
    {

      $from = \DB::table('mubes')
        ->select('*')
        ->where('id','=', $id)
        ->get();

  $ip=\request()->ip();
  date_default_timezone_set("Asia/Jakarta");
  $log = new \App\Models\log_activity;
  $log->id_user = \Auth::user()->id;
  $log->name = \Auth::user()->name;
  $log->email = \Auth::user()->email;
  $log->date_log = date('Y-m-d H:i:s');
  $log->ip = $ip;
  $log->status = 'Update';
  $log->menu = 'Mubes - Update';
  $log->url = url()->full();
  $log->keterangan = 'Update data Mubes : Title : '.$request->title.'
   - Description : '.$request->description.' - Sebelumnya : '.$from;
  $log->save();

        $mubes = \App\Models\mubes::findOrFail($id);
             \DB::table('mubes')->where('id',$id)->update([
                'title' => $request->title,
                'description' => $request->description
                 ]);
                
                 return redirect('/admin/mubes')->withSuccess('success.');
    }
}

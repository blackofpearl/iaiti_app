<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\about;

class aboutController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //if new About
    public function index()
    {
		$role = \DB::table('about')
            ->select('id','desc_remaks','desc_a', 'desc_b', 'youtube')
            ->get();
			
		return view('admin.about.index',['role'=>$role]);
    }    

    //save about
     public function create(Request $request)
     {
 
     $banner = new \App\Models\about;
         $banner->desc_remaks = $request->desc_remaks;
         $banner->desc_a = $request->desc_a;
         $banner->desc_b = $request->desc_b;
         $banner->youtube = $request->youtube;
         $banner->save();
    

         //$ip = $_SERVER['REMOTE_ADDR'];
         //$location = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);
         $ip=\request()->ip();
     date_default_timezone_set("Asia/Jakarta");
     $log = new \App\Models\log_activity;
     $log->id_user = \Auth::user()->id;
     $log->name = \Auth::user()->name;
     $log->email = \Auth::user()->email;
     $log->date_log = date('Y-m-d H:i:s');
     $log->ip = $ip;
     $log->status = 'Create';
     $log->menu = 'About - Create';
     $log->url = url()->full();
     $log->keterangan = 'Membuat Konten About';
     $log->save();
         
     return redirect('/admin/about')->with('sukses','Data Berhasil Buat');
   }

   public function update(Request $request, $id)
    {
      $from = \DB::table('about')
            ->select('id','desc_remaks','desc_a', 'desc_b', 'youtube')
            ->get();

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Update';
      $log->menu = 'About - Update';
      $log->url = url()->full();
      $log->keterangan = 'Update About - [ Desc Remarks : '.$request->desc_remaks.' - Desc a : '.$request->desc_a.' - Desc b : '.$request->desc_b.' - Youtube : '.$request->youtube.'] | Sebelumnya Desc : '.$from;
      $log->save();

            $about = \App\Models\about::findOrFail($id);
            \DB::table('about')->where('id',$id)->update([
                'desc_remaks' => $request->desc_remaks,
                'desc_a' => $request->desc_a,
                'desc_b' => $request->desc_b,
                'youtube' => $request->youtube
                ]);
               
                return redirect('/admin/about')->withSuccess('success.');

    }

    //VM
    public function vm()
    {
		$role = \DB::table('about')
            ->select('id','desc_vm','visi', 'misi')
            ->get();
			
		return view('admin.about.vm',['role'=>$role]);
    }  
    
     //save about VM
     public function vm_create(Request $request)
     {
      $ip=\request()->ip();
     date_default_timezone_set("Asia/Jakarta");
     $log = new \App\Models\log_activity;
     $log->id_user = \Auth::user()->id;
     $log->name = \Auth::user()->name;
     $log->email = \Auth::user()->email;
     $log->date_log = date('Y-m-d H:i:s');
     $log->ip = $ip;
     $log->status = 'Create';
     $log->menu = 'Visi Misi - Create';
     $log->url = url()->full();
     $log->keterangan = 'Membuat Konten Visi Misi';
     $log->save();

     $banner = new \App\Models\about;
         $banner->desc_vm = $request->desc_vm;
         $banner->visi = $request->visi;
         $banner->misi = $request->misi;
         $banner->save();
 
         
     return redirect('/admin/about/vm')->with('sukses','Data Berhasil Buat');
   }

   public function vm_update(Request $request, $id)
    {

      $from = \DB::table('about')
            ->select('id','desc_vm','visi', 'misi')
            ->get();

      $ip=\request()->ip();
      date_default_timezone_set("Asia/Jakarta");
      $log = new \App\Models\log_activity;
      $log->id_user = \Auth::user()->id;
      $log->name = \Auth::user()->name;
      $log->email = \Auth::user()->email;
      $log->date_log = date('Y-m-d H:i:s');
      $log->ip = $ip;
      $log->status = 'Update';
      $log->menu = 'Visi Misi - Update';
      $log->url = url()->full();
      $log->keterangan = 'Update Visi Misi - [ Desc VM : '.$request->desc_vm.' - visi : '.$request->visi.' - misi : '.$request->misi.'] | Sebelumnya Desc : '.$from;
      $log->save();

            $about = \App\Models\about::findOrFail($id);
            \DB::table('about')->where('id',$id)->update([
                'desc_vm' => $request->desc_vm,
                'visi' => $request->visi,
                'misi' => $request->misi
                ]);
               
                return redirect('/admin/about/vm')->withSuccess('success.');

    }

    //so
    public function so()
    {
		$role = \DB::table('about')
            ->select('id','file')
            ->get();
			
		return view('admin.about.so',['role'=>$role]);
    }  

    //save about SO
    public function so_create(Request $request)
    {
      $ip=\request()->ip();
     date_default_timezone_set("Asia/Jakarta");
     $log = new \App\Models\log_activity;
     $log->id_user = \Auth::user()->id;
     $log->name = \Auth::user()->name;
     $log->email = \Auth::user()->email;
     $log->date_log = date('Y-m-d H:i:s');
     $log->ip = $ip;
     $log->status = 'Create';
     $log->menu = 'Struktur Organisasi - Create';
     $log->url = url()->full();
     $log->keterangan = 'Membuat Konten Struktur Organisasi';
     $log->save();

      $validator = Validator::make($request->all(), [
        'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        if ($validator->fails()) {
          return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
        }

      $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/about', $filenameSimpan);

        $banner = new \App\Models\about;
        $banner->file = $filenameSimpan;
        $banner->save();

        
    return redirect('/admin/about/so')->with('sukses','Data Berhasil Buat');
  }

  public function so_update(Request $request, $id)
   {
   
    $validator = Validator::make($request->all(), [
      'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
      ]);
      if ($validator->fails()) {
        return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
      }

    $file = $request->file('file');
		
    $filenameWithExt = $request->file('file')->getClientOriginalName();
    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
    $extension = $request->file('file')->getClientOriginalExtension();
    $filenameSimpan = $filename.'_'.time().'.'.$extension;
    $path = $request->file('file')->storeAs('/public/about', $filenameSimpan);
           $about = \App\Models\about::findOrFail($id);
           $from = \DB::table('about')
    ->select('file')
    ->get();

$ip=\request()->ip();
date_default_timezone_set("Asia/Jakarta");
$log = new \App\Models\log_activity;
$log->id_user = \Auth::user()->id;
$log->name = \Auth::user()->name;
$log->email = \Auth::user()->email;
$log->date_log = date('Y-m-d H:i:s');
$log->ip = $ip;
$log->status = 'Update';
$log->menu = 'Struktur Organisasi - Update';
$log->url = url()->full();
$log->keterangan = 'Update Struktur Organisasi - [ File : '.$filenameSimpan.'] | Sebelumnya Desc : '.$from;
$log->save();

           \DB::table('about')->where('id',$id)->update([
               'file' => $filenameSimpan
               ]);
              
               return redirect('/admin/about/so')->withSuccess('success.');

   }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\banner;

class BannerController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('banner')
            ->select('id','title', 'description', 'link', 'status','file','created_at')
            ->paginate(2);
			
		return view('admin.banner.index',['role'=>$role]);
    }    

    //show
    public function show(Request $request, $id)
    {
		$role = \DB::table('banner')
            ->select('id','title', 'description', 'link', 'status','file','created_at')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.banner.show',['role'=>$role]);
    }    

    //edit
    public function edit(Request $request, $id)
    {
		$role = \DB::table('banner')
        ->select('id','title', 'description', 'link', 'status','file','created_at')
            ->where('id', '=', $id)
            ->get();
			
		return view('admin.banner.edit',['role'=>$role]);
    }    

    //Add Banner
    public function add()
    {
        return view('admin.banner.add');
    }

    //Save Banner
    public function create(Request $request)
    {
		
		$validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
        }
		
		$file = $request->file('file');
		
		$filenameWithExt = $request->file('file')->getClientOriginalName();
		$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
		$extension = $request->file('file')->getClientOriginalExtension();
		$filenameSimpan = $filename.'_'.time().'.'.$extension;
		$path = $request->file('file')->storeAs('/public/banner', $filenameSimpan);

        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $log = new \App\Models\log_activity;
        $log->id_user = \Auth::user()->id;
        $log->name = \Auth::user()->name;
        $log->email = \Auth::user()->email;
        $log->date_log = date('Y-m-d H:i:s');
        $log->ip = $ip;
        $log->status = 'Create';
        $log->menu = 'Banner - Create';
        $log->url = url()->full();
        $log->keterangan = 'Membuat Konten Banner';
        $log->save();

		$banner = new \App\Models\banner;
        $banner->title = $request->title;
        $banner->description = $request->description;
        $banner->link = $request->link;
        $banner->status = $request->status;
        $banner->file = $filenameSimpan;
        $banner->save();

        
		return redirect('/admin/banner')->with('sukses','Data Berhasil Buat');
	}

    //update Banner
    public function update(Request $request, $id)
    {
		
		$validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){

            $from = \DB::table('banner')
            ->select('id','title','description','link','status')
            ->where('id','=', $id)
            ->get();

            $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Update';
            $log->menu = 'Banner - Update';
            $log->url = url()->full();
            $log->keterangan = 'Update Banner : title : '.$request->title.' , Deskripsi : '.$request->description.'
             , Link : '.$request->link.' , Status : '.$request.' |';
            $log->save();

            $banner = \App\Models\banner::findOrFail($id);
            \DB::table('banner')->where('id',$id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'link' => $request->link,
                'status' => $request->status
                ]);
               
                return redirect('/admin/banner/show/'.$banner->id)->withSuccess('success.');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/banner', $filenameSimpan);

            $from = \DB::table('banner')
            ->select('id','title','description','link','status','file')
            ->where('id','=', $id)
            ->get();

            $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Update';
            $log->menu = 'Banner - Update';
            $log->url = url()->full();
            $log->keterangan = 'Update Banner : title : '.$request->title.' , Deskripsi : '.$request->description.'
             , Link : '.$request->link.' , Status : '.$request.' , File : '.$$filenameSimpan.' | Sebelumnya : '.$form;
            $log->save();

            $banner = \App\Models\banner::findOrFail($id);
            \DB::table('banner')->where('id',$id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'link' => $request->link,
                'status' => $request->status,
                'file' => $filenameSimpan
                ]);
               
                return redirect('/admin/banner/show/'.$banner->id)->withSuccess('success.');

        }

	}

    public function destroy($id)
    {
        $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
            $log = new \App\Models\log_activity;
            $log->id_user = \Auth::user()->id;
            $log->name = \Auth::user()->name;
            $log->email = \Auth::user()->email;
            $log->date_log = date('Y-m-d H:i:s');
            $log->ip = $ip;
            $log->status = 'Delete';
            $log->menu = 'Banner - Delete';
            $log->url = url()->full();
            $log->keterangan = 'Menghapus Data Banner';
            $log->save();

    $banner = \App\Models\banner::findOrFail($id);
    $banner->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }
    
}

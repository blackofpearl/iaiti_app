<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tracer;

class tracerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
		$role = \DB::table('tracer')
            ->select('id','title', 'description', 'link', 'status','file','created_at')
            ->paginate(2);
			
		return view('admin.tracer.index',['role'=>$role]);
    }  
    
     //Save tracer
     public function create(Request $request)
     {
        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $log = new \App\Models\log_activity;
        $log->id_user = \Auth::user()->id;
        $log->name = \Auth::user()->name;
        $log->email = \Auth::user()->email;
        $log->date_log = date('Y-m-d H:i:s');
        $log->ip = $ip;
        $log->status = 'Create';
        $log->menu = 'Tracer - Create';
        $log->url = url()->full();
        $log->keterangan = 'Menambah data Tracer';
        $log->save();

         $validator = Validator::make($request->all(), [
             'title' => 'required',
             'description' => 'required',
             'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
         ]);

         if(empty($request->file('file'))){
            
            $tracer = new \App\Models\tracer;
            $tracer->title = $request->title;
            $tracer->description = $request->description;
            $tracer->link = $request->link;
            $tracer->status = $request->status;
            $tracer->save();
    
            
            return redirect('/admin/tracer')->with('sukses','Data Berhasil Buat');
         }else{
            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }
            
            $file = $request->file('file');
            
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/tracer', $filenameSimpan);
    
            $tracer = new \App\Models\tracer;
            $tracer->title = $request->title;
            $tracer->description = $request->description;
            $tracer->link = $request->link;
            $tracer->status = $request->status;
            $tracer->file = $filenameSimpan;
            $tracer->save();
    
            
            return redirect('/admin/tracer')->with('sukses','Data Berhasil Buat');
         }
 
        
     }

     //update tracer
    public function update(Request $request, $id)
    {
		$from = \DB::table('tracer')
      ->select('*')
      ->where('id','=', $id)
      ->get();

$ip=\request()->ip();
date_default_timezone_set("Asia/Jakarta");
$log = new \App\Models\log_activity;
$log->id_user = \Auth::user()->id;
$log->name = \Auth::user()->name;
$log->email = \Auth::user()->email;
$log->date_log = date('Y-m-d H:i:s');
$log->ip = $ip;
$log->status = 'Update';
$log->menu = 'Tracer - Update';
$log->url = url()->full();
$log->keterangan = 'Update data Tracer : Title : '.$request->title.'
 - Description : '.$request->description.'- Link : '.$request->link.'
  - Sebelumnya : '.$from;
$log->save();

		$validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){
            $tracer = \App\Models\tracer::findOrFail($id);
            \DB::table('tracer')->where('id',$id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'link' => $request->link,
                'status' => $request->status
                ]);
               
                return redirect('/admin/tracer')->withSuccess('success.');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/tracer', $filenameSimpan);

            $tracer = \App\Models\tracer::findOrFail($id);
            \DB::table('tracer')->where('id',$id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'link' => $request->link,
                'status' => $request->status,
                'file' => $filenameSimpan
                ]);
               
                return redirect('/admin/tracer')->withSuccess('success.');

        }

	}

}

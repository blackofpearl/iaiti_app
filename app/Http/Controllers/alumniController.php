<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\alumni;

class alumniController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
		$role = \DB::table('alumni_iti')
            ->select('nim as nim','nama as nama','Status as status','TanggalMasuk as masuk', 
            'TanggalLulus as lulus', 'angkatan as akademik','Prodi as prodi','Kelas as kelas')
            ->paginate(6);
			
		return view('admin.alumni.index',['role'=>$role]);
    }    

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$role = \DB::table('alumni_iti')
        ->select('nim as nim','nama as nama','Status as status','TanggalMasuk as masuk', 
        'TanggalLulus as lulus', 'angkatan as akademik','Prodi as prodi','Kelas as kelas')
		->where('nama','like',"%".$cari."%")
		->orWhere('nim','like',"%".$cari."%")
		->orWhere('prodi','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('admin.alumni.index',['role'=>$role]);
 
	}
}

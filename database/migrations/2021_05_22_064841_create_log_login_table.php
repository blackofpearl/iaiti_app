<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogLoginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_login', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->datetime('date_log');
            $table->longText('ip');
            $table->longText('extra_a')->nullable();
            $table->longText('extra_b')->nullable();
            $table->timestamps();
        });

        Schema::table('log_login', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_login');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('id_alumni'); 
			$table->unsignedBigInteger('id_sch');
            $table->text('ms')->nullable();
            $table->text('vs')->nullable();
            $table->string('file');
            $table->string('extra_a')->nullable();
            $table->string('extra_b')->nullable();
            $table->string('extra_c')->nullable();
            $table->timestamps();
        });

        Schema::table('candidate', function (Blueprint $table) {
            $table->foreign('id_alumni')->references('id')->on('alumni_iti');
            $table->foreign('id_sch')->references('id')->on('schedule');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
        Schema::dropIfExists('alumni_iti');
        Schema::dropIfExists('candidate');
    }
}

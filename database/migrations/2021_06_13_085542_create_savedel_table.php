<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSavedelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('savedel', function (Blueprint $table) {
            $table->id();
            $table->string('nim')->nullable();
            $table->string('email')->nullable();
            $table->string('nama')->nullable();
            $table->datetime('del')->nullable();
            $table->string('actor_del')->nullable();
            $table->string('email_del')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('savedel');
    }
}

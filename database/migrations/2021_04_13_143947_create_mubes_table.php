<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMubesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mubes', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description')->nullable();
            $table->timestamps();
        });
		
		DB::table('mubes')->insert([
	            'title' => 'MubesIII',
	            'description' => 'Mubes Adalah',
				'created_at' => '2020-06-23 11:29:31',
				'updated_at' => '2020-06-23 11:29:31'
	        ]
	    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mubes');
    }
}

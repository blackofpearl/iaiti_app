<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_activity', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->Text('name');
            $table->Text('email');
            $table->datetime('date_log');
            $table->longText('ip');
            $table->Text('status');
            $table->Text('menu');
            $table->Text('url');
            $table->longText('keterangan')->nullable();
            $table->longText('extra_a')->nullable();
            $table->longText('extra_b')->nullable();
            $table->longText('extra_c')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_activity');
    }
}

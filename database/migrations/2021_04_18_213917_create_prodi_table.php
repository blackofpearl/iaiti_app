<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prodi', function (Blueprint $table) {
            $table->id();
            $table->integer('code');
            $table->string('name');
            $table->integer('status');
            $table->timestamps();
        });

        DB::table('prodi')->insert([
            'code' => '1',
            'name' => 'Teknik Elektro',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );
        DB::table('prodi')->insert([
            'code' => '2',
            'name' => 'Teknik Mesin',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );
        DB::table('prodi')->insert([
            'code' => '3',
            'name' => 'Teknik Manajemen Industri',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );

        DB::table('prodi')->insert([
            'code' => '4',
            'name' => 'Teknik Kimia',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );
        DB::table('prodi')->insert([
            'code' => '5',
            'name' => 'Teknik Informatika',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );
        DB::table('prodi')->insert([
            'code' => '6',
            'name' => 'Teknik Sipil',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );
        DB::table('prodi')->insert([
            'code' => '7',
            'name' => 'Arsitektur',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );

        DB::table('prodi')->insert([
            'code' => '8',
            'name' => 'Perencanaan Wilayah dan Kota',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );

        DB::table('prodi')->insert([
            'code' => '9',
            'name' => 'Teknologi Industri Pertanian',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );

        DB::table('prodi')->insert([
            'code' => '10',
            'name' => 'Manajemen',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );

        DB::table('prodi')->insert([
            'code' => '11',
            'name' => 'Mekanisasi Pertanian',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prodi');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
			$table->unsignedBigInteger('id_cdt');
            $table->datetime('time_vote')->nullable();
            $table->datetime('status')->nullable();
            $table->string('extra_a')->nullable();
            $table->string('extra_b')->nullable();
            $table->string('extra_c')->nullable();
            $table->timestamps();
        });

        Schema::table('vote', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_cdt')->references('id')->on('candidate');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vote');
        Schema::dropIfExists('users');
        Schema::dropIfExists('candidate');
    }
}

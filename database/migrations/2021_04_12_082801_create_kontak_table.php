<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKontakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontak', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('address');
            $table->text('description')->nullable();
            $table->text('gmaps')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('copyright')->nullable();
            $table->string('design')->nullable();
            $table->string('fb')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->timestamps();
        });
		
		DB::table('kontak')->insert([
	            'title' => 'Kontak Kami',
	            'description' => 'Example Kontak Kami page template',
	            'address' => 'Jl. Raya Puspiptek Serpong,<br/>Tangerang Selatan - Banten',
	            'gmaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.388014751568!2d106.67352851476979!3d-6.343769195409265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e5ab3d01e03b%3A0xb98f4c27e24202ec!2sInstitut%20Teknologi%20Indonesia!5e0!3m2!1sid!2sid!4v1616946966630!5m2!1sid!2sid" frameborder="0"
                    style="border:0" allowfullscreen></iframe>',
	            'phone' => '(021) 7560542 - 7560545',
	            'fax' => '(021) 7560542',
	            'email' => 'info@example.com',
	            'copyright' => 'IA-ITI. All Rights Reserved',
	            'design' => 'IA-ITI Development',
	            'fb' => '#',
	            'twitter' => '#',
	            'instagram' => '#',
	            'linkedin' => '#',
				'created_at' => '2020-06-23 11:29:31',
				'updated_at' => '2020-06-23 11:29:31'
	        ]
	    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontak');
    }
}

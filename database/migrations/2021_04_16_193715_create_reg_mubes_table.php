<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegMubesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reg_mubes', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('id_user');
            $table->string('nim');
            $table->string('name');
            $table->string('prodi');
            $table->integer('angkatan');
            $table->string('phone');
            $table->date('tgl_masuk')->nullable();
            $table->date('tgl_lulus')->nullable();
            $table->string('kelas')->nullable();
            $table->string('extra_a')->nullable();
            $table->longText('extra_b')->nullable();
            $table->string('kode')->nullable();
            $table->integer('status')->nullable();
            $table->string('file');
            $table->timestamps();
        });

        Schema::table('reg_mubes', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('reg_mubes');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumni8499Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumni_84_99', function (Blueprint $table) {
            $table->id();
            $table->string('nim')->nullable();
            $table->string('nama')->nullable();
            $table->string('Prodi')->nullable();
            $table->string('Angkatan')->nullable();
            $table->string('Status')->nullable();
            $table->string('TanggalMasuk')->nullable();
            $table->string('TanggalLulus')->nullable();
            $table->string('Thn Akademik')->nullable();
            $table->string('Kelas')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumni_84_99');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKatGalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('status');
            $table->timestamps();
        });
		
		DB::table('kategori')->insert([
	            'name' => 'Mabes',
	            'status' => 1,
				'created_at' => '2020-06-23 11:29:31',
				'updated_at' => '2020-06-23 11:29:31'
	        ]
	    );

		DB::table('kategori')->insert([
            'name' => 'Alumni',
            'status' => 1,
            'created_at' => '2020-06-23 11:29:31',
            'updated_at' => '2020-06-23 11:29:31'
            ]
        );

        Schema::create('galery', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('kategori_id');
            $table->string('title');
            $table->text('description');
            $table->string('file');
            $table->integer('status');
            $table->timestamps();
        });

        Schema::table('galery', function (Blueprint $table) {
            $table->foreign('kategori_id')->references('id')->on('kategori');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('galery');
		Schema::drop('kategori');
    }
}

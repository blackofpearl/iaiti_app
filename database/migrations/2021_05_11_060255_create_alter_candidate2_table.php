<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlterCandidate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE candidate MODIFY ms  LONGTEXT;');
        DB::statement('ALTER TABLE candidate MODIFY vs  LONGTEXT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE candidate MODIFY ms  varchar;');
        DB::statement('ALTER TABLE candidate MODIFY vs  varchar;');
    }
}

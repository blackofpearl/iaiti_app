<?php
$hit = \DB::table('log_login')
->where('id_user', '=', Auth::user()->id )
->where('date_log', '=', date('Y-m-d'))
->get();
$hit = $hit->count();

    date_default_timezone_set("Asia/Jakarta");
    $ip = $_SERVER['REMOTE_ADDR'];
    $location = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);
    $location;

$banner = new \App\Models\log_login;
$banner->id_user = Auth::user()->id;
$banner->date_log = date('Y-m-d H:i:s');
$banner->ip = $location;
$banner->save();

?>

<script type="text/javascript">
window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		theme: "light2",
		title:{
			text: "Jumlah Per Prodi"
		},		
		data: [
		{       
			type: "pie",
			showInLegend: true,
			toolTipContent: "{y} - #percent %",
			yValueFormatString: "#,#### People",
			legendText: "{indexLabel}",
			dataPoints: [
                <?php
                $role = \DB::table('reg_mubes')
                ->select('prodi', DB::raw('COUNT(nim) as jml'))
                ->groupBy('prodi')
                ->orderBy('jml','DESC')
                ->get();
                ?>
                @foreach($role as $aa)
				{  y: {{$aa->jml}}, indexLabel: "{{$aa->prodi}} : {{$aa->jml}}" },
                @endforeach
			]
		}
		]
	});
	chart.render();
}
</script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-graduation-cap"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Alumni</span>
                <span class="info-box-number">
                    <?php
                        $role = \DB::table('alumni_iti')
                        ->select('nim')
                        ->get();
                        echo count($role);
                    ?>
                  <small>Orang</small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-lime elevation-1"><i class="fas fa-file-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Register</span>
                <span class="info-box-number">
                <?php
                        $reg = \DB::table('reg_mubes')
                        ->select('nim')
                        ->get();
                        echo count($reg);
                    ?>
                  <small>Orang</small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-sign-in-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Approved</span>
                <span class="info-box-number">
                <?php
                        $reg1 = \DB::table('reg_mubes')
                        ->select('nim')
                        ->whereBetween('status', array(1, 2))
                        ->get();
                        echo count($reg1);
                    ?>
                    <small>Orang</small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Waitting</span>
                <span class="info-box-number">
                <?php
                        $reg1 = \DB::table('reg_mubes')
                        ->select('nim')
                        ->where('status','=',0)
                        ->get();
                        echo count($reg1);
                    ?>
                    <small>Orang</small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
		<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title">Jumlah Total By PRODI 

                <?php
                $role = \DB::table('mubes')
                ->select('title')
                ->get();
                
                foreach($role as $na){
                echo $na->title;
                }

                ?>
                </h5>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-center">
                      <strong><?php /*
            $role = \DB::table('mubes')
            ->select('title')
            ->get();
            
            foreach($role as $na){
              echo $na->title;
            }

            ?> 
            <?php
                  
                  date_default_timezone_set("Asia/Jakarta");
                  $rolesaa = \DB::table('schedule')
                    ->select('*')
                    ->where('extra_a','=', '1')
                    ->where('periode_b','>=', date('Y-m-d H:i:s'))
                    ->get();
                  ?>
                    @foreach ($rolesaa as $na)
                   <b>{{$na->periode_a}}</b> , Di Tutup Pada : <b>{{$na->periode_b}}</b>
                    @endforeach
            </strong>
            */ ?>
                    </p>

                    <div class="chart">
                      
                    <div class="progress-group">
                      
						<div id="chartContainer" style="height: 300px; width: 100%;">

                    </div>

                    </div>
                    <!-- /.chart-responsive -->
                  </div>
                 
                  
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title">Kandidat 

                <?php
                $role = \DB::table('mubes')
                ->select('title')
                ->get();
                
                foreach($role as $na){
                echo $na->title;
                }

                ?>
                
                 Periode : </h5>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="#" class="dropdown-item">Action</a>
                      <a href="#" class="dropdown-item">Another action</a>
                      <a href="#" class="dropdown-item">Something else here</a>
                      <a class="dropdown-divider"></a>
                      <a href="#" class="dropdown-item">Separated link</a>
                    </div>
                  </div>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-center">
                      <strong><?php /*
            $role = \DB::table('mubes')
            ->select('title')
            ->get();
            
            foreach($role as $na){
              echo $na->title;
            }

            ?> : 1 juli, 2020 - 30 Ags, 2020</strong>
            */ ?>
                    </p>

                    <div class="chart">

                    <?php

                  $kl = \DB::table('candidate')
                  ->join('schedule','schedule.id','=','candidate.id_sch')
                  ->join('reg_mubes','reg_mubes.id','=','candidate.id_alumni')
                  ->select('reg_mubes.name as nama','candidate.id as id')
                  ->where('schedule.status','<>','2')
                  ->get();
                    ?>
                    @foreach($kl as $l)
                    <div class="progress-group">
                      {{$l->nama}}
                      <span class="float-right"><b>
                      <?php
                        $ttl = \DB::table('vote')
                        ->select('id_cdt')
                        ->where('id_cdt', '=', $l->id)
                        ->get();
                        $t=count($ttl);
                        echo count($ttl);
                        ?>
                      </b>/
                        <?php
                        $role = \DB::table('reg_mubes')
                        ->select('nim')
                        ->whereBetween('status', array(1, 2))
                        ->get();
                        $c=count($role);
                        echo count($role); 
                        ?>
                      </span>
                      <div class="progress progress-sm">
                        <div class="progress-bar bg-yellow" style="width:<?php echo $t/$c*100; ?>%"></div>
                      </div>
                    </div>
                    @endforeach

                    </div>
                    <!-- /.chart-responsive -->
                  </div>
                 
                  
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
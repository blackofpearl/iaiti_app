@extends('adminlte::page')

@section('name', 'listuser')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>New Users Mubes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item"><a href="/admin/listuser2">listuser</a></li>
              <li class="breadcrumb-item active">Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

<div class="card card-default">
          <div class="card-header">

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <style>
            .close {
            cursor: pointer;
            position: absolute;
            padding: 12px 16px;
            transform: translate(0%, -50%);
            }
        </style>
          @if(count($errors) > 0)
				<div class="alert alert-danger">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
                </center>
				</div>
				@endif
                <script>
                var closebtns = document.getElementsByClassName("close");
                var i;

                for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function() {
                    this.parentElement.style.display = 'none';
                });
                }
                </script>
        <form class="form" action="<?php echo url("/admin/listuser2/create2"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <?php
           $ip = $_SERVER['REMOTE_ADDR'];
           $location = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);
           $loca
          ?>
          <input type="hidden" name="extra_b" value="{{$location}}">
          <div class="card-body">

          <div class="row">
          <div class="col-6 col-sm-6">

			  <div class="form-group">
                  <label>NIM *</label>
                <input type="text" class="form-control" id="nim" name="nim" placeholder="nim" required>
                </div>
                <!-- /.form-group -->
            </div>

            <div class="col-6 col-sm-6">
              <div class="form-group">
                  <label>Fullname *</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Fullname" required>
                </div>
              <!-- /.col -->
            </div>
            
            <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Username *</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
                </div>
                <!-- /.form-group -->
            </div>

            <div class="col-6 col-sm-6">
              <div class="form-group">
                  <label>Prodi *</label>
                  <select name="prodi" id="prodi" class="form-control validate" required>
                <?php
					$prodi = DB::table('prodi')
                    ->select('name','code')
                    ->groupBy('code','name')
                    ->get();
					foreach ($prodi as $name) {
				?> 
                          
                 <option value="{{$name->name}}" >{{$name->name}}
                 </option>
				 <?php
				}
				?>
                </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Email *</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email " required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Angkatan * (Year : <?php echo date('Y'); ?> )</label>
                  <input type="tel" pattern="[0-9]{4}" name="angkatan" class="form-control" id="angkatan" placeholder="Input Tahun Angkatan anda" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Telephone * (Format :+62-8xx-xxxxxxxx)</label><br/>
                  <input type="text"  name='phone1' size='1' id="phone1" value='+62' > - 
                  <input type="text"  name="phone2" size='2' id="phone2" placeholder="8xx" required> - 
                  <input type="text"  size='8' name='phone3' id="phone3" placeholder="xxxxxx" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Foto * (Type: JPG, Jpeg, Png, gif)</label>
                  <input type="file" name="file" class="form-control" id="file" placeholder="Input Foto Anda" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Status </label>
                  <select name="extra_a" id="extra_a" class="form-control validate">
                 <option value="LULUS">LULUS</option>
                 <option value="TIDAK LULUS" >TIDAK LULUS</option>
                </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Password *</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="password " required>
                <!-- hide id_role -->
                <input type="hidden" class="form-control" id="id_role" name="id_role" value='4'>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Submit</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
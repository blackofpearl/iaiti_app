@extends('adminlte::page')

@section('name', 'listuser')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>listuser</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item"><a href="/admin/listuser2">listuser</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

<div class="card card-default">
          <div class="card-header">

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <style>
            .close {
            cursor: pointer;
            position: absolute;
            padding: 12px 16px;
            transform: translate(0%, -50%);
            }
        </style>
          @if(count($errors) > 0)
				<div class="alert alert-danger">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
                </center>
				</div>
				@endif
                <script>
                var closebtns = document.getElementsByClassName("close");
                var i;

                for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function() {
                    this.parentElement.style.display = 'none';
                });
                }
                </script>
        @foreach($role as $na)
        <form class="form" action="<?php echo url("/admin/listuser2/update2/{$na->id}"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <?php
           $ip = $_SERVER['REMOTE_ADDR'];
           $location = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);
           $loca
          ?>
          <input type="hidden" name="extra_b" value="{{$location}}">
          <div class="card-body">

          <div class="row">
          <div class="col-6 col-sm-6">

			  <div class="form-group">
                  <label>NIM *</label>
                <input type="text" class="form-control" id="nim" name="nim" value="{{$na->nim}}" required>
                </div>
                <!-- /.form-group -->
            </div>

            <div class="col-6 col-sm-6">
              <div class="form-group">
                  <label>Fullname *</label>
                <input type="text" class="form-control" id="name" name="name" value="{{$na->fullname}}" required>
                </div>
              <!-- /.col -->
            </div>
            
            <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Username *</label>
                <input type="text" class="form-control" id="username" name="username" value="{{$na->name}}" required>
                </div>
                <!-- /.form-group -->
            </div>

            <div class="col-6 col-sm-6">
              <div class="form-group">
                  <label>Prodi *</label>
                  <select name="prodi" id="prodi" class="form-control validate" required>
                <?php
					$prodi = DB::table('prodi')
                    ->select('name','code')
                    ->groupBy('code','name')
                    ->get();
					foreach ($prodi as $name) {
				?> 
                          
                 <option value="{{$name->name}}"  {{$na->prodi ==  $name->name  ? 'selected' : ''}}>{{$name->name}}
                 </option>
				 <?php
				}
				?>
                </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Email *</label>
                <input type="email" class="form-control" id="email" name="email" value="{{$na->email}} " required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Angkatan *</label>
                  <input type="tel" pattern="[0-9]{4}" name="angkatan" class="form-control" id="angkatan" value="{{$na->angkatan}}" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Telephone * (Format :+62-8xx-xxxxxxxx)</label><br/>
                  <?php 
                    $c=substr($na->phone, 0,3);
                    $a=substr($na->phone, 3, 3);
                    $b=substr($na->phone, 6);
                  ?>
                  <input type="text" name='phone1'  id="phone1" size='1'  value="{{$c}}" required> - 
                  <input type="text" name='phone2'  size='2' id="phone2" value="{{$a}}" required> - 
                  <input type="text" name='phone3'  size='8' id="phone3" value="{{$b}}" required>
                  </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Foto</label>
                  <img src="{{asset('storage/profil/'.$na->file)}}" alt="{{$na->file}}" width='90px' class="img-circle img-fluid">
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Status </label>
                  <select name="extra_a" id="extra_a" class="form-control validate">
                 <option value="LULUS" {{'LULUS' ==  $na->extra_a  ? 'selected' : ''}}>LULUS</option>
                 <option value="TIDAK LULUS" {{'TIDAK LULUS' ==  $na->extra_a  ? 'selected' : ''}}>TIDAK LULUS</option>
                </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Foto  (JPG, Jpeg, Png, gif)</label>
                  <input type="file" name="file" class="form-control" id="file" value="Input Foto Anda" >
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-6 col-sm-6">
                <div class="form-group">
                  <label>Approval </label>
                  <select name="status" id="status" class="form-control validate">
                 <option value="0" {{'0' ==  $na->status  ? 'selected' : ''}}>Menunggu</option>
                 <option value="2" {{'2' ==  $na->status  ? 'selected' : ''}}>Approval By Manual</option>
                 <option value="3" {{'3' ==  $na->status  ? 'selected' : ''}}>Tidak Di Setujui</option>
                </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-5 col-sm-6">
                <div class="form-group">
                  <label>Password </label>
                <input type="password" class="form-control" id="password" name="password" placeholder="password " >
                <!-- hide id_role -->
                <input type="hidden" class="form-control" id="id_role" name="id_role" value='4'>
                <input type="hidden" class="form-control" id="kode" name="kode" value='{{$na->code}}'>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Submit</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
        @endforeach
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
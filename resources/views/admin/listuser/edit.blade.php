@extends('adminlte::page')

@section('name', 'listuser')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>listuser</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item"><a href="/admin/listuser">listuser</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

<div class="card card-default">
          <div class="card-header">

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <style>
            .close {
            cursor: pointer;
            position: absolute;
            padding: 12px 16px;
            transform: translate(0%, -50%);
            }
        </style>
          @if(count($errors) > 0)
				<div class="alert alert-danger">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
                </center>
				</div>
				@endif
                <script>
                var closebtns = document.getElementsByClassName("close");
                var i;

                for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function() {
                    this.parentElement.style.display = 'none';
                });
                }
                </script>
        @foreach($role as $na)
        <form class="form" action="<?php echo url("/admin/listuser/update/{$na->id}"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">

          <div class="row">
          <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Username *</label>
                <input type="text" class="form-control" id="name" name="name" Value="{{$na->name}}">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Email *</label>
                <input type="email" class="form-control" id="email" name="email" Value="{{$na->email}} ">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Password *</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="password ">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
              <div class="form-group">
                  <label>Role</label>
                  <select name='id_role' class="form-control select2bs4" style="width: 100%;">
                    <?php
				    $roles = DB::table('roles')
                        ->select('*')
                        ->where('id','<','4')
                        ->get();
					foreach ($roles as $name) {
					?> 
                    <option value='{{$name->id}}' {{$na->id_role ==  $name->id  ? 'selected' : ''}}>{{$name->role_user}}</option>
                    <?php
                    }
                    ?>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Submit</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
        @endforeach
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
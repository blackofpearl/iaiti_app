@extends('adminlte::page')

@section('role_user', 'candidate')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>candidate</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item"><a href="/admin/kandidat">candidate</a></li>
              <li class="breadcrumb-item active">Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')
@foreach($role as $g) 
<div class="card card-default">
          <div class="card-header">

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
          
        <form class="form" action="<?php echo url("/admin/kandidat/update/$g->id"); ?>" method="POST" enctype="multipart/form-data">
            <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">

          <div class="row">
              

              <div class="col-12 col-sm-12">
              <div class="form-group">
                  <label>Schedule</label>
                  
                  <select name='id_sch' class="form-control select2bs4"  id="select-state" name='id_sch' placeholder="Pick a Schedule..." required>
                    <option value="">Select a Periode Mubes...</option>
                    <?php
                      $aa = \DB::table('schedule')
                                ->select('*')
                                ->get();
                      foreach ($aa as $aals) {

                    ?> 
                    <option value="{{$aals->id}}" {{$aals->id ==  $g->id_sch  ? 'selected' : ''}}>{{$aals->title}} | {{$aals->periode_a}}</option>
                    <?php
                      }
                    ?>
                    
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
              <div class="form-group">
                  <label>Kandidat</label>
                  
                  <select name='id_alumni' class="form-control select2bs4"  id="select-state" name='id_alumni' placeholder="Pick a Candidate..." required>
                    <option value="">Select a Candidate...</option>
                    <?php
                      $aa = \DB::table('reg_mubes')
                                ->select('*')
                                ->get();
                      foreach ($aa as $aals) {
                    ?> 
                    <option value="{{$aals->id}}" {{$aals->id ==  $g->id_alumni  ? 'selected' : ''}}>{{$aals->nim}} | {{$aals->name}}</option>
                    <?php
                      }
                    ?>                    
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Visi </label>
                  <textarea class="form-control" name='vs' rows="3" placeholder="Visi ..." >{{$g->vs}}</textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Misi</label>
                  <textarea class="form-control" name='ms' rows="3" placeholder="Misi ..." >{{$g->ms}}</textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              
              <div class="col-12 col-sm-12">
              <div class="form-group">
                  <label>Icon</label>
                <input type="file" class="form-control" id="file" name="file" placeholder="Upload Icon">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Submit</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
@endforeach
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <script>
      $(document).ready(function () {
      $('select').selectize({
          sortField: 'text'
      });
  });
  </script>
@stop
@extends('adminlte::page')

@section('title', 'Tracer')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tracer</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item active">Tracer</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

@if(count($role) <= 0)
<div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Tracer Study</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
          
                <form class="form" action="<?php echo url("/admin/tracer/create"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Judul *</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="judul" required>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Status</label>
                  <select name='status' class="form-control select2bs4" style="width: 100%;">
                    <option value='1' selected="selected">Aktif</option>
                    <option value='2'>Non Aktif</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Link</label>
                  <input type="text" class="form-control" id="link" name="link" placeholder="link">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Upload Gambar *</label>
                  <input type="file" class="form-control" id="file" name="file" placeholder="upload Banner">                  
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Deskripsi *</label>
                  <textarea class="form-control" name='description' rows="3" placeholder="Deskripsi ..." required></textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Submit</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
@else

@foreach($role as $g)
<!-- IF NULL -->
<div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Welcome Remaks</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif

                <form class="form" action="<?php echo url("/admin/tracer/update/$g->id"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Judul *</label>
                  <input type="text" class="form-control" id="title" name="title"value="{{$g->title}}" required>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                <label>Deskripsi *</label>
                  <textarea class="form-control" name='description' rows="7" required>{{$g->description}}</textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->


              <div class="col-md-6">
                <div class="form-group">
                  <label>Link</label>
                  <input type="text" class="form-control" id="link" name="link" value="{{$g->link}}">
                </div>

                <div class="form-group">
                  @if(empty($g->file))
                  <center><h3><b>Belum Upload Gambar Tracer Study</b></h3></center>
                  @else
                  <img alt="img" class="table" src="{{asset('storage/tracer/'.$g->file)}}">
                  @endif
                </div>

                <!-- /.form-group -->
                <div class="form-group">
                  <label>Upload Gambar *</label>
                  <input type="file" class="form-control" id="file" name="file">                  
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group">
                <label>Status</label>
                  <select name='status' class="form-control select2bs4" style="width: 100%;">
                    <option value='1' {{'1' ==  $g->status  ? 'selected' : ''}}>Aktif</option>
                    <option value='2' {{'2' ==  $g->status  ? 'selected' : ''}}>Non Aktif</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Update</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
@endforeach

@endif

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
@extends('adminlte::page')

@section('title', 'Form Banner')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Formulir Banner</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item"><a href="/admin/banner">Banner</a></li>
              <li class="breadcrumb-item active">add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

<div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Banner</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
          
        <form class="form" action="<?php echo url("/admin/banner/create"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Judul *</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="judul" required>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Status</label>
                  <select name='status' class="form-control select2bs4" style="width: 100%;">
                    <option value='1' selected="selected">Aktif</option>
                    <option value='2'>Non Aktif</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Link</label>
                  <input type="text" class="form-control" id="link" name="link" placeholder="link">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Upload Gambar *</label>
                  <input type="file" class="form-control" id="file" name="file" placeholder="upload Banner" required>                  
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Deskripsi *</label>
                  <textarea class="form-control" name='description' rows="3" placeholder="Deskripsi ..." required></textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Submit</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
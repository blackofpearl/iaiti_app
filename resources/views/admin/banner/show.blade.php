@extends('adminlte::page')

@section('title', 'Show Banner')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banner</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item"><a href="/admin/banner">Banner</a></li>
              <li class="breadcrumb-item active">Detail</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')
       <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
          <b>Detail</b> 
          @foreach($role as $g) 
            {{$g->title}}
         
          </h3>

          <div class="card-tools">
          <a class="btn btn-info btn-sm" href="<?php echo url('/admin/banner/edit/'.$g->id); ?>">
                              <i class="fas fa-edit">
                              </i>
                          </a>
                          @endforeach
          <a class="btn btn-warning btn-sm" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
           <i class="fas fa-minus"></i></a>
           &nbsp;
          </div>
        </div>
        <div class="card-body p-0">
        @foreach($role as $g)
        <div class='row'>
            <div class="col-md-6">
            <img alt="img" class="table" src="{{asset('storage/banner/'.$g->file)}}">
            </div>
            <div class="col-md-6">
            Link : 
            @if(!empty($g->link))
                          <a href='{!!$g->link!!}' target='_blank'>{{$g->link}}</a>
                      @endif
                      @if(empty($g->link))
                          <a href='#'>Tidak Ada Link</a>
                      @endif
            <br/>
            Status : 
            @if($g->status == 1)
                          <span class="badge badge-success">Aktif</span>
                      @endif
                      @if($g->status == 2)
                          <span class="badge badge-danger">Non Aktif</span>
                      @endif
                      <br/>
            Deskripsi : {{$g->description}}
            </div>
        </div>
        @endforeach
        </div>
        <!-- /.card-body -->

      </div>
      <!-- /.card -->
      
      
    </section>
    <!-- /.content -->
    
            </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style>
    .bs-example{
        margin: 20px;        
    }
</style>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
@stop
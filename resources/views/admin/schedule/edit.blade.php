@extends('adminlte::page')

@section('role_user', 'schedule')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>schedule</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item"><a href="/admin/schedule">schedule</a></li>
              <li class="breadcrumb-item active">Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')
@foreach($role as $g) 
<div class="card card-default">
          <div class="card-header">

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
          
        <form class="form" action="<?php echo url("/admin/schedule/update/$g->id"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">

          <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>schedule *</label>
                <input type="text" class="form-control" id="title" name="title" value="{{$g->title}}" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Start *</label>
                <input type="datetime-local" class="form-control" id="periode_a" name="periode_a" value="{{$g->periode_a}}" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Close *</label>
                <input type="datetime-local" class="form-control" id="periode_b" name="periode_b" value="{{$g->periode_b}}" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col --> 

              <div class="col-12 col-sm-12">
              <div class="form-group">
                  <label>Type</label>
                  <select name='extra_a' class="form-control select2bs4" style="width: 100%;">
                    <option value='1'  {{'1' ==  $g->status  ? 'selected' : ''}}>Register</option>
                    <option value='2'  {{'2' ==  $g->status  ? 'selected' : ''}}>Candidate</option>
                    <option value='3'  {{'3' ==  $g->status  ? 'selected' : ''}}>Vote</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-12 col-sm-12">
              <div class="form-group">
                  <label>Status</label>
                  <select name='status' class="form-control select2bs4" style="width: 100%;">
                    <option value='1' {{'1' ==  $g->status  ? 'selected' : ''}}>Open</option>
                    <option value='2' {{'2' ==  $g->status  ? 'selected' : ''}}>Close</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Submit</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
@endforeach
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
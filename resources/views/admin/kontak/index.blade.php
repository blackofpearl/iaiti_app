@extends('adminlte::page')

@section('title', 'kontak')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>kontak</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item active">kontak</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

@if(count($role) <= 0)
<div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Kontak Kami</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
          
        <form class="form" action="<?php echo url("/admin/kontak/create"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">

          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Judul</label>
                  <input type="text" class="form-control" id="title" name="title" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-6">
                <div class="form-group">
                  <label>fax</label>
                  <input type="text" class="form-control" id="fax" name="fax" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Descriptions</label>
                  <textarea class="form-control" name='description' rows="3"  required></textarea>
                </div>
                
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>gmaps</label>
                  <textarea class="form-control" name='gmaps' rows="3"  ></textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>phone</label>
                  <input type="text" class="form-control" id="phone" name="phone"  >
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>email</label>
                  <input type="email" class="form-control" id="email" name="email"  >
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>copyright</label>
                  <textarea class="form-control" name='copyright' rows="3"  required></textarea>
                </div>
                
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>address</label>
                  <textarea class="form-control" name='address' rows="3"  required></textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
			  
			  <div class="col-md-6">
                <div class="form-group">
                  <label>design</label>
                  <input type="text" class="form-control" id="design" name="design"  required>
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>fb</label>
                  <input type="text" class="form-control" id="fb" name="fb" >
                </div>
                <!-- /.form-group -->
              </div>
			  
			  <div class="col-md-6">
                <div class="form-group">
                  <label>twitter</label>
                  <input type="text" class="form-control" id="twitter" name="twitter" >
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>instagram</label>
                  <input type="text" class="form-control" id="instagram" name="instagram"  >
                </div>
                <!-- /.form-group -->
              </div>
			  
			  <div class="col-md-12">
                <div class="form-group">
                  <label>linkedin</label>
                  <input type="text" class="form-control" id="linkedin" name="linkedin" >
                </div>
                <!-- /.form-group -->
              </div>
			  
            </div>
            <!-- /.row -->

           

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Update</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
@else

@foreach($role as $g)
<!-- IF NULL -->
<div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">{{$g->title}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
          
        <form class="form" action="<?php echo url("/admin/kontak/update/$g->id"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">

          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Judul</label>
                  <input type="text" class="form-control" id="title" name="title" value='{{$g->title}}' required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-6">
                <div class="form-group">
                  <label>fax</label>
                  <input type="text" class="form-control" id="fax" name="fax" value='{{$g->fax}}' required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Descriptions</label>
                  <textarea class="form-control" name='description' rows="3"  required>{{$g->description}}</textarea>
                </div>
                
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>gmaps</label>
                  <textarea class="form-control" name='gmaps' rows="3"  required>{{$g->gmaps}}</textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>phone</label>
                  <input type="text" class="form-control" id="phone" name="phone" value='{{$g->phone}}' required>
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>email</label>
                  <input type="email" class="form-control" id="email" name="email" value='{{$g->email}}' required>
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>copyright</label>
                  <textarea class="form-control" name='copyright' rows="3"  required>{{$g->copyright}}</textarea>
                </div>
                
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>address</label>
                  <textarea class="form-control" name='address' rows="3"  required>{{$g->address}}</textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
			  
			  <div class="col-md-6">
                <div class="form-group">
                  <label>design</label>
                  <input type="text" class="form-control" id="design" name="design" value='{{$g->design}}' required>
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>fb</label>
                  <input type="text" class="form-control" id="fb" name="fb" value='{{$g->fb}}' required>
                </div>
                <!-- /.form-group -->
              </div>
			  
			  <div class="col-md-6">
                <div class="form-group">
                  <label>twitter</label>
                  <input type="text" class="form-control" id="twitter" name="twitter" value='{{$g->twitter}}' required>
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>instagram</label>
                  <input type="text" class="form-control" id="instagram" name="instagram" value='{{$g->instagram}}' required>
                </div>
                <!-- /.form-group -->
              </div>
			  
			  <div class="col-md-12">
                <div class="form-group">
                  <label>linkedin</label>
                  <input type="text" class="form-control" id="linkedin" name="linkedin" value='{{$g->linkedin}}' required>
                </div>
                <!-- /.form-group -->
              </div>
			  
            </div>
            <!-- /.row -->

           

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Update</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>
@endforeach

@endif

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
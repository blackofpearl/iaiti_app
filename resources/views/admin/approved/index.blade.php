@extends('adminlte::page')

@section('title', 'Waiting')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Waiting </h1>

          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item active">List Users</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

<section class="content">

<!-- Default box -->
<div class="card card-solid">
<style>
            .close {
            cursor: pointer;
            position: absolute;
            padding: 12px 16px;
            transform: translate(0%, -50%);
            }
        </style>
         @if(count($errors) > 0)
				<div class="alert alert-danger">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
                </center>
				</div>
            	@endif
        @if(!empty(\Session::get('success')) > 0)
           <div class="alert alert-success">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
                            {!! \Session::get('success') !!}</li>
                </center>
                    </div>
        @endif
                <script>
                var closebtns = document.getElementsByClassName("close");
                var i;

                for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function() {
                    this.parentElement.style.display = 'none';
                });
                }
                function checkAll(ele) {
                        var checkboxes = document.getElementsByTagName('input');
                        if (ele.checked) {
                            for (var i = 0; i < checkboxes.length; i++) {
                                if (checkboxes[i].type == 'checkbox') {
                                    checkboxes[i].checked = true;
                                }
                            }
                        } else {
                            for (var i = 0; i < checkboxes.length; i++) {
                                console.log(i)
                                if (checkboxes[i].type == 'checkbox') {
                                    checkboxes[i].checked = false;
                                }
                            }
                        }
                    }
                </script>
          <div class="col-12 col-sm-12 col-lg-12">
          All : <INPUT type="checkbox" onchange="checkAll(this)" name="chk[]" />
                   
            <div class="card card-primary card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Cari Nama</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Cari By Prodi</a>
                  </li>
                 
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">

                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                  <form action="/admin/approved/cari" method="GET">
                     <input class="form-control validate" type="text" name="cari" placeholder="Cari User .." value="{{ old('cari') }}">
                    <input type="submit" class="form-control validate" value="Search">
                  </form>  
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                  <form action="/admin/approved/cari2" method="GET">
                      <select class="form-control validate" name="cari2" id="cari2">
                      <?php
                        $prodi = DB::table('prodi')
                                  ->select('name','code')
                                  ->groupBy('code','name')
                                  ->get();
                        foreach ($prodi as $name) {
                      ?> 
                                        
                              <option value="{{$name->name}}" >{{$name->name}}
                              </option>
                      <?php
                      }
                      ?>
                      </select>
                      <input class="form-control validate" type="submit" value="Search">
                    </form>
                  </div>
                  
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
                    
  <div class="card-body pb-0">
    <form class="form" action="<?php echo url("/admin/approved/all"); ?>" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="row d-flex align-items-stretch">
     @foreach($role as $g)
      <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
        <div class="card bg-light">
          <div class="card-header text-muted border-bottom-0">
        <INPUT type="checkbox" value='{{$g->id}}' name="chk[]"/>
        <INPUT type="hidden" value='{{$g->fullname}}' name="username[]"/>
        <INPUT type="hidden" value='{{$g->nim}}' name="nim[]"/>
        <INPUT type="hidden" value='{{$g->code}}' name="kode[]"/>
        <INPUT type="hidden" value='{{$g->email}}' name="email[]"/>
          {{$g->role_user}}
          </div>
          <div class="card-body pt-0">
            <div class="row">
              <div class="col-7">
                <h2 class="lead"><b>{{$g->fullname}}</b></h2>
                <ul class="ml-1 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span>{{$g->nim}} </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> {{$g->phone}} </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-clock"></i></span> {{$g->angkatan}} | <i class="fas fa-lg fa-Thumbs-up"></i></span>{{$g->extra_a}}</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>{{$g->email}} </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-book"></i></span>{{$g->prodi}} </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-id-card"></i></span>
                        <?php 
          $id = Auth::user()->id;
                $roless = \DB::table('user_roles')
                ->select('id_role')
                ->where('id_user','=',$id)
                ->get();
          ?>
          @foreach($roless as $lv)
          @if($lv->id_role == 1)
          {{$g->code}}
          @endif
          @endforeach    
                        </li>
                </ul>
                
              </div>
              <div class="col-5 text-center">
                <img src="{{asset('storage/profil/'.$g->file)}}" alt="{{$g->file}}" width='100%' class="img-circle img-fluid">
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="text-right">
            <?php 
              if($g->status == 0){
              ?>
                <a href="#" class="btn btn-sm bg-warning">
                <i class="fas fa-layer-group"></i >Menunggu
                </a>
              <?php
              }else if($g->status == 1){
              ?>
                <a href="#" class="btn btn-sm bg-success">
                <i class="fas fa-layer-group"></i> Approval by System
                </a>
              <?php
              }else if($g->status == 2){
                ?>
                  <a href="#" class="btn btn-sm bg-success">
                  <i class="fas fa-layer-group"></i> Approval by Manual
                  </a>
                <?php
                }else{
                ?>
                  <a href="#" class="btn btn-sm bg-danger">
                  <i class="fas fa-layer-group"></i> Tidak Di Setujui
                  </a> 
                  <?php               
                }
            ?>
         
              <a href="/admin/approved/edit/{{$g->id}}" class="btn btn-sm bg-teal">
                <i class="fas fa-edit"></i>
              </a>

              
              @if($g->id > 4)
              <a href="<?php echo url('/admin/approved/destroy/'.$g->id); ?>" class="btn btn-danger btn-sm confirmation"><i class="fas fa-trash"></i></a>
													<script type="text/javascript">
													var elems = document.getElementsByClassName('confirmation');
													var confirmIt = function (e) {
														if (!confirm('Anda yakin ingin hapus data ini?')) e.preventDefault();
													};
													for (var i = 0, l = elems.length; i < l; i++) {
														elems[i].addEventListener('click', confirmIt, false);
													}
													</script>
              @endif
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    <select name="status" id="status" class="form-validate">
                 <option value="0" >Menunggu</option>
                 <option value="2" >Approval By Manual</option>
                 <option value="3" >Tidak Di Setujui</option>
                </select>
    <button type="submit" class="btn btn-primary">Approved</button> 
      </form>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <nav aria-label="Contacts Page Navigation">
      <ul class="pagination justify-content-center m-0">
        <li class="page-item">{!! $role->links() !!}</li>
      </ul>
    </nav>
  </div>
  <!-- /.card-footer -->
</div>
<!-- /.card -->

</section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style>
    .bs-example{
        margin: 20px;        
    }
</style>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
@stop
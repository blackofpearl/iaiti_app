@extends('adminlte::page')

@section('title', 'List Alumni')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List Alumni</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item active">List Alumni</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

<section class="content">

<!-- Default box -->
<div class="card card-solid">
<style>
            .close {
            cursor: pointer;
            position: absolute;
            padding: 12px 16px;
            transform: translate(0%, -50%);
            }
        </style>
        @if(!empty(\Session::get('success')) > 0)
           <div class="alert alert-success">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
                            {!! \Session::get('success') !!}</li>
                </center>
                    </div>
        @endif
                <script>
                var closebtns = document.getElementsByClassName("close");
                var i;

                for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function() {
                    this.parentElement.style.display = 'none';
                });
                }
                </script>

          <div class="col-12">
              <div class="card-header text-muted border-bottom-0">
              <form action="/admin/alumni/cari" method="GET">
                <input type="text" name="cari" placeholder="Cari User .." value="{{ old('cari') }}">
                <input type="submit" value="Search">
              </form>
              </div>
          </div>  

  <div class="card-body pb-0">
    <div class="row d-flex align-items-stretch">

    @foreach($role as $g)
      <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
        <div class="card bg-light">
          <div class="card-header text-muted border-bottom-0">
          Data Alumni
          </div>
          <div class="card-body pt-0">
            <div class="row">
              <div class="col-7">
                <h2 class="lead"><b>{{$g->nama}}</b></h2>
                <ul class="ml-1 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span>{{$g->nim}} </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> {{$g->status}} </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-clock"></i></span> {{$g->akademik}} </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>{{$g->prodi}} </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-book"></i></span>{{$g->kelas}} </li>
                </ul>
                
              </div>
              <div class="col-5 text-center">
                <img src="{{ asset('banner/default/users.jpg') }}" alt="" class="img-circle img-fluid">
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="text-right">
            IA-ITI
            </div>
          </div>
        </div>
      </div>
      @endforeach

    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <nav aria-label="Contacts Page Navigation">
      <ul class="pagination justify-content-center m-0">
        <li class="page-item">{!! $role->links() !!}</li>
      </ul>
    </nav>
  </div>
  <!-- /.card-footer -->
</div>
<!-- /.card -->

</section>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style>
    .bs-example{
        margin: 20px;        
    }
</style>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
@stop
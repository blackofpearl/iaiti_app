@extends('adminlte::page')

@section('title', 'galery')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>galery</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item active">galery</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')
       <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">galery</h3>

          <div class="card-tools">
          <a class="btn btn-warning btn-sm" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
           <i class="fas fa-minus"></i></a>
           &nbsp;
           <a class="btn btn-success btn-sm" href="/admin/galery/add">Add
                              <i class="fas fa-pencil-alt">
                              </i>
                          </a>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects" id="example2">
              <thead>
                  <tr>
                     <th style="width: 30%">
                          Foto
                      </th>
                      <th>
                         Title & Kategori
                      </th>
                      <th style="width: 20%">
                          Description
                      </th>
                      <th style="width: 8%" class="text-center">
                          Status
                      </th>
                      <th style="width: 20%">
                      </th>
                  </tr>
              </thead>
              <tbody>
			  @foreach($role as $g)
                  <tr>
                      <td>  
                        <img alt="img" class="table" src="{{asset('storage/galery/'.$g->file)}}">
                      </td>
                      <td>
                          <a>
                              {{$g->title}} | {{$g->kat_name}}
                          </a>
                          <br/>
                          <small>
                              Created {{$g->created_at}}
                          </small>
                      </td>
                      <td class="project_progress">
                          
                          <small>
                          <?php
											$kalimat = $g->description;
											$sub_kalimat = substr($kalimat,0,110);
											echo $sub_kalimat.'. . .';
										?>
                          </small>
                      </td>
                      <td class="project-state">
                      @if($g->status == 1)
                          <span class="badge badge-success">Aktif</span>
                      @endif
                      @if($g->status == 2)
                          <span class="badge badge-danger">Non Aktif</span>
                      @endif
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-primary btn-sm" href="<?php echo url('/admin/galery/show/'.$g->id); ?>">
                              <i class="fas fa-search">
                              </i>
                          </a>
                          <a class="btn btn-info btn-sm" href="<?php echo url('/admin/galery/edit/'.$g->id); ?>">
                              <i class="fas fa-edit">
                              </i>
                          </a>
                          <a href="<?php echo url('/admin/galery/destroy/'.$g->id); ?>" class="btn btn-danger btn-sm confirmation"><i class="fas fa-trash"></i></a>
													<script type="text/javascript">
													var elems = document.getElementsByClassName('confirmation');
													var confirmIt = function (e) {
														if (!confirm('Anda yakin ingin hapus data ini?')) e.preventDefault();
													};
													for (var i = 0, l = elems.length; i < l; i++) {
														elems[i].addEventListener('click', confirmIt, false);
													}
													</script>
                      </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->

      </div>
      <!-- /.card -->
      
      
    </section>
    <!-- /.content -->
    <div class="col-md-12">
                <div class="form-group">
                  
                <div class="bs-example">
                    <nav>
                        <ul class="pagination">
                            <li class="page-item">
                                {!! $role->links() !!}
                            </li>
                        </ul>
                    </nav>
                </div>

                </div>
            </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style>
    .bs-example{
        margin: 20px;        
    }
</style>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
@stop
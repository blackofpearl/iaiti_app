@extends('adminlte::page')

@section('title', 'Form galery')

@section('content_header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Formulir galery</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/home">Admin</a></li>
              <li class="breadcrumb-item"><a href="/admin/galery">galery</a></li>
              <li class="breadcrumb-item active">update</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@stop

@section('content')

@foreach($role as $g) 
<div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Update 
            {{$g->title}}
            </h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>

          @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
          
        <form class="form" action="<?php echo url("/admin/galery/update/$g->id"); ?>" method="POST" enctype="multipart/form-data">
          <!-- /.card-header -->
          {{csrf_field()}}
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Judul *</label>
                  <input type="text" class="form-control" id="title" name="title" value="{{$g->title}}" required>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Status</label>
                  <select name='status' class="form-control select2bs4" style="width: 100%;">
                    <option value='1' {{'1' ==  $g->status  ? 'selected' : ''}}>Aktif</option>
                    <option value='2' {{'2' ==  $g->status  ? 'selected' : ''}}>Non Aktif</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kategori</label>
                  <select name='kategori_id' class="form-control select2bs4" style="width: 100%;">
                  <?php
                  $rolesaa = \DB::table('kategori')
                    ->select('*')
                    ->get();
                  ?>
                    @foreach ($rolesaa as $na)
                    <option value='{{$na->id}}' {{$na->id ==  $g->kategori_id  ? 'selected' : ''}}>{{$na->name}}</option>
                    @endforeach
                  </select>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Upload Gambar *</label>
                  <input type="file" class="form-control" id="file" name="file" placeholder="upload galery" >                  
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label>Deskripsi *</label>
                  <textarea class="form-control" name='description' rows="3" required>{{$g->description}}</textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-12">
                <div class="form-group"><center>
                    <button type="submit" class="btn btn-primary">Update</button>       
                </center></div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            IA-ITI 
          </div>
        </div>
        <!-- /.card -->
        </form>

@endforeach

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <meta name="description" content="Using 3D FlipBook jQuery plugin with regular jQuery syntax">
    <meta name="keywords" content="3d flipbook live example,real3d flipbook live example,real 3d flipbook live example,flipbook js live example,flipbook live example">
   
<link rel="stylesheet" href="{{ asset('flip/css/style.css') }}">
<script src="{{ asset('flip/js/jquery.min.js') }}"></script>
  </head>

  <body>
	
    <div class="site">
      
      <div class="container">
        <div class="content">
         
<div class="sample-container">

</div>


<script src="{{ asset('flip/js/three.min.js') }}"></script>
<script src="{{ asset('flip/js/pdf.min.js') }}"></script>

<script src="{{ asset('flip/js/3dflipbook.min.js') }}"></script>

<script type="text/javascript">
var jq = $.noConflict();
  jq('.sample-container').FlipBook({pdf: 'https://3dflipbook.net/books/pdf/CondoLiving.pdf'});
</script>
        </div>
        <div class="copyright">
          <span class="sign">©</span> <span class="author">iberezansky</span> All rights reserved
        </div>
      </div>
    </div>
  </body>
</html>
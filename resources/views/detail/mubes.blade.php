@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')

<section id="team" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Mubes</h2>
          <ol>
            <li><a href="/">Home</a></li>
            <li>Mubes</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs Section -->

    <section id="team"  class="team">
      <div class="container">
        @foreach($role as $g)
        <center>
        <div class='row'>
        <div class="col-md-12">
            <b><h2><p style="text-align:center;">{!!$g->title!!}<p></h2></b>
            <?php
                  
                  date_default_timezone_set("Asia/Jakarta");
                  $rolesaa = \DB::table('schedule')
                    ->select('*')
                    ->where('extra_a','=', '1')
                    ->where('periode_b','>=', date('Y-m-d H:i:s'))
                    ->get();
                  ?>
                    @foreach ($rolesaa as $na)
                   <h4> {{$na->title}} <br/> Di Buka Pada : <b>{{$na->periode_a}}</b> , Di Tutup Pada : <b>{{$na->periode_b}}</b></h4>
                    @endforeach
                    
                    Date Now :
                    <?php 
                    echo date('d-m-Y H:i:s');
                    ?>
            </div>
            <div class="col-md-12">
            <b>Keterangan : </b><br/>
            <p style="text-align:center;">{{$g->description}}</p>
            </div>
            <br/>
            <div class="col-md-12">
            <b>Link Register : </b><br/>
            <?php
                  $rolesaa = \DB::table('schedule')
                    ->select('*')
                    ->where('extra_a','=', '1')
                    ->where('periode_b','>=', date('Y-m-d H:i:s'))
                    ->get();
                  ?>
                  
                  @if(count($rolesaa) <= 0) Tidak ada regristasi untuk saat ini! @endif
                    @foreach ($rolesaa as $na)
                    <a href="<?php echo url('/Register-Mubes'); ?>" class="btn-get-started animate__animated animate__fadeInUp scrollto">Register {!!$g->title!!}</a>
                  @endforeach
            </div>
        </div>
        @endforeach
        </div>
    </section>

  </main><!-- End #main -->

@endsection
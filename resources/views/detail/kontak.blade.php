@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')

<section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Kontak Kami</h2>
          <ol>
            <li><a href="/">Home</a></li>
            <li>Kontak Kami</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs Section -->

    <section id="contact"  class="contact">
      <div class="container">
        @foreach($role as $g)
        <div class='row'>
        <div class="col-md-12">
            <b>Alamat : </b><br/>
            <p style="text-align:justify;">{!!$g->address!!}<p>
            <br/>
            </div>
            <div class="col-md-12">
            <b>Kami adalah : </b><br/>
            <p style="text-align:justify;">{{$g->description}}</p>
            </div>
            <br/>
            <div class="col-md-12">
            <b>Phone : </b><br/>
            <p style="text-align:justify;">{{$g->phone}}</p>
            <b>Fax : </b><br/>
            <p style="text-align:justify;">{{$g->fax}}</p>
            <b>email : </b><br/>
            <p style="text-align:justify;">{{$g->email}}</p>
            </div>
        </div>
        @endforeach
        </div>
    </section>

  </main><!-- End #main -->

@endsection
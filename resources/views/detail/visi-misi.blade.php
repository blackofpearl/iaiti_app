@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')

<section id="vs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Visi - Misi</h2>
          <ol>
            <li><a href="/">Home</a></li>
            <li>Visi - Misi</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs Section -->

    <section class="vs">
      <div class="container">
        @foreach($role as $g)
        <div class='row'>
        <div class="col-md-12">
            <b>Sasaran Stategis dan Indikator Kinerja : </b><br/>
            <p style="text-align:justify;">{{$g->desc_vm}}<p>
            <br/>
            <br/>
            </div>
            <div class="col-md-12">
            <b>Visi : </b><br/>
            <p style="text-align:justify;">{{$g->visi}}</p>
            </div>
            <br/>
            <div class="col-md-12">
            <br/>
            <br/>
            <b>Misi : </b><br/>
            <p style="text-align:justify;">{{$g->misi}}</p>
            </div>
        </div>
        @endforeach
        </div>
    </section>

  </main><!-- End #main -->

@endsection
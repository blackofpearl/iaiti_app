@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')

<section id="banner" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Detail Banner</h2>
          <ol>
            <li><a href="/">Home</a></li>
            <li>Detail Banner</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs Section -->

    <section class="banner">
      <div class="container">
        @foreach($role as $g)
        <div class='row'>
            <div class="col-md-12">
            <img alt="img" class="table" src="{{asset('storage/banner/'.$g->file)}}">
            </div>
            <div class="col-md-12">
            <b>Link : </b>
            @if(!empty($g->link))
                          <a href='{!!$g->link!!}' target='_blank'>{{$g->link}}</a>
                      @endif
                      @if(empty($g->link))
                          <a href='#'>Tidak Ada Link</a>
                      @endif
            <br/>
            <p style="text-align:justify;"> {{$g->description}} </p>
            </div>
        </div>
        @endforeach
        </div>
    </section>

  </main><!-- End #main -->

@endsection
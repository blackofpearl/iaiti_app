@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')

<section id="tracer" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Tracer Studi</h2>
          <ol>
            <li><a href="/">Home</a></li>
            <li>Tracer Studi</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs Section -->

    <section class="tracer">
      <div class="container">
        @foreach($role as $g)
        <div class='row'>
        <div class="col-md-12">
            <b> {{$g->title}} </b><br/>
            <br/>
            <p style="text-align:justify;">{{$g->description}}<p>
            </div>
            <div class="col-md-12">
            <b>link : </b><br/>
            <p style="text-align:justify;">{{$g->link}}</p>
            </div>
            <div class="col-md-12">
            <br/>
            <b>Hasil Tracer : </b><br/>
            <img alt="img" class="table" src="{{asset('storage/tracer/'.$g->file)}}">
            </div>
        </div>
        @endforeach
        </div>
    </section>

  </main><!-- End #main -->

@endsection
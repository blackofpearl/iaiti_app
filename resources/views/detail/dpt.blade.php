@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')


<script type="text/javascript">
window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		theme: "light2",
		title:{
			text: "Jumlah Per Prodi"
		},		
		data: [
		{       
			type: "pie",
			showInLegend: true,
			toolTipContent: "{y} - #percent %",
			yValueFormatString: "#,#### People",
			legendText: "{indexLabel}",
			dataPoints: [
                <?php
                $role = \DB::table('reg_mubes')
                ->select('prodi', DB::raw('COUNT(nim) as jml'))
                ->groupBy('prodi')
                ->orderBy('jml','DESC')
                ->get();
                ?>
                @foreach($role as $aa)
				{  y: {{$aa->jml}}, indexLabel: "{{$aa->prodi}} : {{$aa->jml}}" },
                @endforeach
			]
		}
		]
	});
	chart.render();
}
</script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <!-- /.row -->
		<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header"><center>
              <?php
                  
                  date_default_timezone_set("Asia/Jakarta");
                  $rolesaa = \DB::table('schedule')
                    ->select('*')
                    ->where('extra_a','=', '1')
                    ->where('periode_b','>=', date('Y-m-d H:i:s'))
                    ->get();
                  ?>
                    @foreach ($rolesaa as $na) 
                    <font color='black'>Masa Pendaftaran : <b>{{$na->periode_a}}</b> , Di Tutup Pada : <b>{{$na->periode_b}}</b></font>
                    @endforeach
                    </center>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-center">
                      <strong><?php /*
            $role = \DB::table('mubes')
            ->select('title')
            ->get();
            
            foreach($role as $na){
              echo $na->title;
            }

            ?> 
            <?php
                  
                  date_default_timezone_set("Asia/Jakarta");
                  $rolesaa = \DB::table('schedule')
                    ->select('*')
                    ->where('extra_a','=', '1')
                    ->where('periode_b','>=', date('Y-m-d H:i:s'))
                    ->get();
                  ?>
                    @foreach ($rolesaa as $na)
                   <b>{{$na->periode_a}}</b> , Di Tutup Pada : <b>{{$na->periode_b}}</b>
                    @endforeach
            </strong>
            */ ?>
                    </p>

                    <div class="chart">
                      
                    <div class="progress-group">
                      
						<div id="chartContainer" style="height: 300px; width: 100%;">

                    </div>

                    </div>
                    <!-- /.chart-responsive -->
                  </div>
                 
                  
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>

  </main><!-- End #main -->

@endsection
@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')

<section id="so" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Struktur Organisasi</h2>
          <ol>
            <li><a href="/">Home</a></li>
            <li>Struktur Organisasi</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs Section -->

    <section class="so">
      <div class="container">
        @foreach($role as $g)
        <div class='row'>
        <div class="col-md-12">
            <img alt="img" class="table" src="{{asset('storage/about/'.$g->file)}}">
            </div>
        </div>
        @endforeach
        </div>
    </section>

  </main><!-- End #main -->

@endsection
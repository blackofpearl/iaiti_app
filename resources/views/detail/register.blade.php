@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')

<?php
                  
                  date_default_timezone_set("Asia/Jakarta");
                  $rolesaa = \DB::table('schedule')
                    ->select('*')
                    ->where('extra_a','=', '1')
                    ->where('periode_b','>=', date('Y-m-d H:i:s'))
                    ->get();
                  ?>

                  
                    @if(count($rolesaa) <= 0) <center><h3><b>Tidak ada regristasi untuk saat ini!</b></h3></center> @endif
                    @foreach ($rolesaa as $na)
                   
                   
<section id="team" class="breadcrumbs">
      <div class="container">
        
      @foreach($role as $g)
        <div class="d-flex justify-content-between align-items-center">
          <h2>{{$g->title}}</h2>
          <ol>
            <li><a href="/">Home</a></li>
            <li>{{$g->title}}</li>
          </ol>
        </div>
        @endforeach
      </div>
    </section><!-- End Breadcrumbs Section -->

      <section id="team" class="team abouta">
      <div class="container" data-aos="fade-up">

        <div class="row no-gutters">
        <style>
            .close {
            cursor: pointer;
            position: absolute;
            padding: 12px 16px;
            transform: translate(0%, -50%);
            }
        </style>
        @if(count($errors) > 0)
				<div class="alert alert-danger">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
                </center>
				</div>
            	@endif

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
                            {!! \Session::get('success') !!}</li>
                </center>
                    </div>
                @endif
                
                <script>
                var closebtns = document.getElementsByClassName("close");
                var i;

                for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function() {
                    this.parentElement.style.display = 'none';
                });
                }
                </script>
		  <div class="col-lg-6 d-flex flex-column justify-content-center about-content">
			<div class="section-title">
			  <h2>Formulir Registrasi</h2>
        <p>
        <?php
                  
                  date_default_timezone_set("Asia/Jakarta");
                  $rolesaa = \DB::table('schedule')
                    ->select('*')
                    ->where('extra_a','=', '1')
                    ->where('periode_b','>=', date('Y-m-d H:i:s'))
                    ->get();
                  ?>
                    @foreach ($rolesaa as $na) 
                    <font color='white'>Masa Pendaftaran : <b>{{$na->periode_a}}</b> , Di Tutup Pada : <b>{{$na->periode_b}}</b></font>
                    @endforeach
        </P>
			</div>
    <?php
    $ip = $_SERVER['REMOTE_ADDR'];
    $location = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);
    $location;
    ?>

			<div class="row">

            <form class="form" action="<?php echo url("/registrasi/create"); ?>" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            
            <input type="hidden" name="extra_b" value="{{$location}}">
                <div class="col-lg-12 form-group">
				<label><b>NIM</b> Format : (xxxyyqqqq) xxx kode Prodi, yy kode tahun dan qqqq kode seri</label>
                  <input type="text" name="nim" class="form-control" id="nim" placeholder="Input NIM Anda" >
                </div><br/>
                <div class="col-lg-12 form-group">
				<label><b>NAMA LENGKAP *</b></label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Input Nama Lengkap Anda" required>
                </div><br/>
                <div class="col-lg-12 form-group">
				<label><b>NAMA PANGGILAN *</b></label>
                  <input type="text" name="username" class="form-control" id="username" placeholder="Input Nama Panggilan Anda" required>
                </div><br/>
				<div class="col-lg-12 form-group">
				<label><b>PROGRAM STUDI *</b></label>
                <select name="prodi" id="prodi" class="form-control validate">
                <?php
					$prodi = DB::table('prodi')
                    ->select('name','code')
                    ->groupBy('code','name')
                    ->get();
					foreach ($prodi as $name) {
				?> 
                          
                 <option value="{{$name->name}}" >{{$name->name}}
                 </option>
				 <?php
				}
				?>
                </select>
                </div><br/>
				<div class="col-lg-12 form-group">
				<label><b>ANGKATAN *</b> (Year : <?php echo date('Y'); ?> )</label>
                  <input type="tel" pattern="[0-9]{4}" name="angkatan" class="form-control" id="angkatan" placeholder="Input Tahun Angkatan anda" required>
                </div><br/>
				<div class="col-lg-12 form-group">
				<label><b>EMAIL *</b></label>
                  <input type="email" name="email" class="form-control" id="email" placeholder="Input Email Anda" required>
                </div><br/>
				<div class="col-lg-12 form-group">
				<label><b>TELEPHONE *</b> (Format :+62-[989]-[989900])</label><br/>
                  <input type="tel" pattern="[+][0-9]{1,3}" name='phone1' id="phone1" size='1' placeholder='+62' > - 
                  <input type="tel" pattern="[0-9]{1,9}" name="phone2" size='2' id="phone2" placeholder="xxx" required> - 
                  <input type="tel" pattern="[0-9]{4,8}" name='phone3' size='8' id="phone3" placeholder="xxxxx" required>
                </div><br/>
        <div class="col-lg-12 form-group">
				<label><b>STATUS *</b></label>
                <select name="extra_a" id="extra_a" class="form-control validate">
                 <option value="LULUS" >LULUS</option>
                 <option value="TIDAK LULUS" >TIDAK LULUS</option>
                </select>
                </div><br/>
				<div class="col-lg-12 form-group">
				<label><b>FOTO *</b> (Type: JPG, Jpeg, Png, gif | ukuran foto max 1 Mb,Foto yang tidak menampakan wajah akan dihapus secara otomatis)</label>
                  <input type="file" name="file" class="form-control" id="file" placeholder="Input Foto Anda" required>
                </div><br/>
				
              
              <div class="text-center"><button type="submit">Kirim</button></div>
			  
            </form>
			
			</div>

          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center about-content">
			<div class="section-title">
			  <h2>Informasi</h2>
			</div>
			
			<div class="row">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class='bx bxs-receipt bx-fade-down'></i></div>
              <h4 class="title"><a href="">Tahap 1</a></h4>
              <p class="description">Dilengkapi semua data dengan baik dan benar</p>
            </div>
			
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class='bx bxs-check-shield bx-fade-down'></i></div>
              <h4 class="title"><a href="">Tahap 2</a></h4>
              <p class="description">Pihak Admin akan melakukan pengecekan pada data anda, jika telah di acc, anda akan di beri tahu user dan password login</p>
            </div>
			
			<div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class='bx bxs-user-detail bx-fade-down'></i></div>
              <h4 class="title"><a href="">Tahap 3</a></h4>
              <p class="description">Anda akan masuk di dalam aplikasi dan informasi selanjutnya akan ada di dalam aplikasi setelah login</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class='bx bxs-envelope bx-fade-down'></i></div>
              <h4 class="title"><a href="">Recovery Code</a></h4>
              <p class="description">
              <form  action="<?php echo url("/recovery"); ?>" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <label><b>&nbsp; &nbsp; &nbsp; EMAIL *</b></label>
                  <input type="email" name="email" id="email" placeholder="Input Email Anda" required>
                  <button type="submit">Kirim</button>
                  </form></p>
            </div>
			
			</div>

          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

  </main><!-- End #main -->


                   @endforeach

@endsection
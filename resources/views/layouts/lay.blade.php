<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>@yield('title')</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ URL::asset('assets/img/Logo-IAITI.png') }}" rel="icon">
  <link href="{{ URL::asset('assets/img/Logo-IAITI.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ URL::asset('assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Author: Angga Ibnu Saputra
  * License: https://bootstrapmade.com/license/Angga
  ======================================================== -->
</head>

<body>
<header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center">

      <div class="logo me-auto">
        <h1><a href="#"><img src="assets/img/Logo-IAITI.png" class="img-fluid" alt=""></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="/#hero">Beranda</a></li>
		  <li class="dropdown"><a href="#"><span>Tentang Kami</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a class="nav-link scrollto" href="/#about">Welcome Remarks</a></li>
              <li><a class="nav-link scrollto" href="/detail/visi-misi">Visi Dan Misi</a></li>
              <li><a class="nav-link scrollto" href="/detail/so">Struktur Organisasi</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="/tracer">Tracer Study</a></li>
          <li><a class="nav-link scrollto" href="/#services">Majalah</a></li>
          <li><a class="nav-link scrollto" href="/#portfolio">Galeri IA-ITI</a></li>
          <li><a class="nav-link scrollto" href="/kontak/#contact">Kontak Kami</a></li>
          <li><a class="nav-link scrollto" href="/mubes/#team">
          <?php
          $mubes = DB::table('mubes')
		  ->select('*')
          ->get();
          ?>
		  @foreach ($mubes as $mbs)
            {{$mbs->title}}
          @endforeach
          </a></li>
          <!--
		  <li class="nav-link scrollto">
          <input type="search" placeholder="Search">
        </li> -->
      </ul>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

@yield('container')

  <!-- ======= Footer ======= -->
  <?php
          $kontak = DB::table('kontak')
		  ->select('*')
          ->get();
          ?>
		  @foreach ($kontak as $ktk)
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

		<div class="col-lg-3 col-md-6 footer-info">
            <h3>{{$ktk->title}}</h3>
            <p>
            {!!$ktk->address!!}<br><br>
              <strong>Phone:</strong> {{$ktk->phone}}<br>
			  <strong>Fax:</strong> {{$ktk->fax}}<br/>
              <strong>Email:</strong> {{$ktk->email}}<br>
            </p>
            <div class="social-links mt-3">
              <a href="{{$ktk->twitter}}" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="{{$ktk->fb}}" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="{{$ktk->instagram}}" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="{{$ktk->linkedin}}" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>
		
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a class="nav-link scrollto" href="#hero">Beranda</a></li>
              <li><i class="bx bx-chevron-right"></i> <a class="nav-link scrollto" href="#about">Tentang Kami</a></li>
              <li><i class="bx bx-chevron-right"></i> <a class="nav-link scrollto" href="#service">Majalah</a></li>
              <li><i class="bx bx-chevron-right"></i> <a class="nav-link scrollto" href="#portfolio">Galeri IA-ITI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a class="nav-link scrollto" href="#">Kontak Kami</a></li>
              <li><i class="bx bx-chevron-right"></i> <a class="nav-link scrollto" href="#">Mubes III</a></li>
            </ul>
          </div>
				  
          <div class="col-lg-6 col-md-6 footer-newsletter">
		  <style>
		  .map-container{
			overflow:hidden;
			padding-bottom:56.25%;
			position:relative;
			height:0;
			}
			.map-container iframe{
			left:0;
			top:0;
			height:100%;
			width:100%;
			position:absolute;
			}
		   </style>
            <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 100%">
            {!!$ktk->gmaps!!}
			</div>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright {!!$ktk->copyright!!}
      </div>
      <div class="credits">
        Designed by <a href="#">{!!$ktk->design!!}</a>
      </div>
    </div>
  </footer>
  @endforeach
  <!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ URL::asset('assets/vendor/aos/aos.js') }}"></script>
  <script src="{{ URL::asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ URL::asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
  <script src="{{ URL::asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ URL::asset('assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ URL::asset('assets/vendor/purecounter/purecounter.js') }}"></script>
  <script src="{{ URL::asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ URL::asset('assets/js/main.js') }}"></script>

</body>

</html>
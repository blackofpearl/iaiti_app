@extends('layouts/lay')

@section('title', 'IA-ITI')

@section('container')


  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background-image: url('assets/img/slide/tes1.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Welcome to <span>IA-ITI</span></h2>
                <p class="animate__animated animate__fadeInUp">Selamat Datang Di Website Ikatan Alumni Institut Teknologi Indonesia.</p>
                </div>
            </div>
          </div>
          <?php
          $banner = DB::table('banner')
		  ->select('*')
          ->where('status', '=', 1)
          ->get();
          ?>
		  @foreach ($banner as $bn) 
          <!-- Slide 2 -->
          <div class="carousel-item" style="background-image: url('{{asset('storage/banner/'.$bn->file)}}');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">{{$bn->title}}</h2>
                <p class="animate__animated animate__fadeInUp">
                <?php
				$kalimat = $bn->description;
				$sub_kalimat = substr($kalimat,0,110);
				echo $sub_kalimat.'. . .';
				?>
                </p>
                <a href="<?php echo url('/detail/banner/'.Crypt::encryptString($bn->id)); ?>" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div>
            @endforeach
        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
          <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
          <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <?php
          $about = DB::table('about')
		  ->select('*')
          ->get();
          ?>
		  @foreach ($about as $ab) 
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="row no-gutters">
          <div class="col-lg-6 video-box">
            <img src="assets/img/about1.jpg" class="img-fluid" alt="">
            <a href="{{$ab->youtube}}" target="_blank" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            <h2>Tentang Kami</h2>
              <p>{{$ab->desc_remaks}}</p>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center about-content">

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="#">Tujuan IAITI</a></h4>
              <p class="description">{{$ab->desc_a}}</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="#">Manfaat IAITI</a></h4>
              <p class="description">{{$ab->desc_b}}</p>
            </div>

          </div>
        </div>

      </div>
    </section>
    @endforeach
    <!-- End About Us Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Majalah</h2>
          <h3>COMING SOON</h3>
        </div>
        <!--
        <div class="row">
          <div class="container mt-5 mb-5">
		<div class="d-flex justify-content-center row">
			<div class="col-md-10">
				<div class="row p-2 bg-white border rounded">
					<div class="col-md-3 mt-1"><img class="img-fluid img-responsive rounded product-image" src="https://i.imgur.com/QpjAiHq.jpg"></div>
					<div class="col-md-8 mt-1">
						<h5>Quant olap shirts</h5>
						<div class="d-flex flex-row">
						</div>
						<p class="text-justify">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.<br><br></p>
					</div>
				   
				</div>
				<div class="row p-2 bg-white border rounded mt-2">
					<div class="col-md-3 mt-1"><img class="img-fluid img-responsive rounded product-image" src="https://i.imgur.com/JvPeqEF.jpg"></div>
					<div class="col-md-8 mt-1">
						<h5>Quant trident shirts</h5>
						<div class="d-flex flex-row">
						</div>
						<p class="text-justify text-truncate para mb-0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.<br><br></p>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>


	<div class="text-center"><button type="submit">Lainnya</button></div>
        </div>
        -->
      </div>
    </section><!-- End Services Section -->

    <!-- ======= Our Portfolio Section ======= -->
    <section id="portfolio" class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="section-title">
          <h2>Galeri Foto IA-ITI</h2>
          <p>Berikut di bawah ini kumpulan galeri foto - foto para alumni dan berbagai acara yang di adakan oleh Ikatan Alumni Institut Teknologi Indonesia</p>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <?php
          $kategori = DB::table('kategori')
		  ->select('*')
          ->get();
          ?>
		  @foreach ($kategori as $ktg)
              <li data-filter=".filter-{{$ktg->id}}">{{$ktg->name}}</li>
          @endforeach

            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

        <?php
          $galery = DB::table('galery')
		  ->select('*')
          ->where('status', '=', 1)
          ->get();
          ?>
		  @foreach ($galery as $gly)

          <div class="col-lg-4 col-md-6 portfolio-item filter-{{$gly->kategori_id}}">
            <div class="portfolio-wrap">
              <img src="{{asset('storage/galery/'.$gly->file)}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>{{$gly->title}}</h4>
                <p>{{$gly->title}}</p>
                <div class="portfolio-links">
                  <a href="{{asset('storage/galery/'.$gly->file)}}" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="{{$gly->title}}"><i class="bi bi-plus"></i></a>
                  <a href="<?php echo url('/detail/galery/'.Crypt::encryptString($gly->id)); ?>" title="More Details"><i class="bi bi-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          @endforeach

        </div>

      </div>
    </section><!-- End Our Portfolio Section -->

  </main><!-- End #main -->

@endsection
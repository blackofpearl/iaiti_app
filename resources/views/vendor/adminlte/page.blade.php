@extends('adminlte::master')

@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

@section('classes_body', $layoutHelper->makeBodyClasses())

@section('body_data', $layoutHelper->makeBodyData())

@section('body')
    <div class="wrapper">

        {{-- Top Navbar --}}
        @if($layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.navbar.navbar-layout-topnav')
        @else
            @include('adminlte::partials.navbar.navbar')
        @endif

        <?php

$uri = $_SERVER['REQUEST_URI'];
$id = Auth::user()->id;
$role = \DB::table('users')
->join('user_roles','users.id','=','user_roles.id_user')
->join('roles','roles.id','=','user_roles.id_role')
->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user', 'user_roles.id_role as id_roles')
->where('users.id', '=', $id)
->get();

foreach($role as $na){

if($na->id_roles == 1){
?>
        
        {{-- Left Main Sidebar --}}
        @if(!$layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.sidebar.left-sidebar')
        @endif

<?php
}else if($na->id_roles == 2){
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
<a href="" class="brand-link ">
<img src="/assets/img/Logo-IAITI.png" alt="IAITI" class="brand-image img-circle elevation-3" style="opacity:.8">

    
    <span class="brand-text font-weight-light ">
        <b>IA</b>ITI
    </span>

</a>
    
    
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column "
                data-widget="treeview" role="menu">
                
                <li  class="nav-item">
                    <a class="nav-link <?php if($uri == '/admin/home'){ echo'active'; } ?> 
                    " href="/admin/home" >
                    <i class="fas fa-fw fa-tachometer-alt "></i>
                    <p>beranda<span class="badge badge-success right">Home</span></p>
                    </a>
                </li> 
                <li  class="nav-item">
                            <a class="nav-link 
                            <?php if($uri == '/admin/banner'){ echo'active'; } ?>
                            " href="/admin/banner" >
                            <i class="fas fa-fw fa-scroll "></i>
                            <p> Banner</p>
                            </a>
                </li> 
                <li  class="nav-item">
                                    <a class="nav-link <?php if($uri == '/admin/about'){ echo'active'; } ?>
                                    " href="/admin/about"        >
                                    <i class="fas fa-fw fa-hand-paper "></i>
                                    <p> Welcome Remarks  </p>
                                    </a>
                </li> 
                <li  class="nav-item">
                                    <a class="nav-link <?php if($uri == '/admin/about/vm'){ echo'active'; } ?>
                                    " href="/admin/about/vm">
                                    <i class="fas fa-fw fa-place-of-worship "></i>
                                    <p> Visi &amp; Misi
                                    </p>
                                    </a>
                </li>
                <li  class="nav-item">
                                    <a class="nav-link <?php if($uri == '/admin/about/so'){ echo'active'; } ?> 
                                    "href="/admin/about/so"        >
                                    <i class="fas fa-fw fa-sitemap "></i>
                                    <p>Struktur Organisasi </p>
                                    </a>
                </li> 
                <li  class="nav-item">
                                    <a class="nav-link <?php if($uri == '/admin/tracer'){ echo'active'; } ?> 
                                     " href="/admin/tracer"        >
                                    <i class="fas fa-fw fa-school "></i>
                                    <p>Tracer Study</p>
                                    </a>
                </li> 

                <li  class="nav-item">
                                    <a class="nav-link  <?php if($uri == '/admin/majalah'){ echo'active'; } ?>
                                    " href="/admin/majalah">
                                    <i class="fas fa-fw fa-book "></i>
                                    <p>Majalah</p>
                                    </a>
                                </li> 

                                <li  class="nav-item">
                                            <a class="nav-link <?php if($uri == '/admin/galery'){ echo'active'; } ?>     
                                            " href="/admin/galery"        >
                                            <i class="fas fa-fw fa-clipboard-list "></i>
                                            <p>List Galeri</p>
                                            </a>
                                </li> 

                                <li  class="nav-item ">
                                        <a class="nav-link <?php if($uri == '/admin/kategori'){ echo'active'; } ?>
                                         " href="/admin/kategori" >
                                        <i class="fas fa-fw fa-certificate "></i>
                                        <p> Kategori </p></a>
                                </li> 
                                <li  class="nav-item">
                                        <a class="nav-link  <?php if($uri == '/admin/prodi'){ echo'active'; } ?>
                                        " href="/admin/prodi" >
                                            <i class="fas fa-fw fa-building "></i>
                                            <p> Prodi </p>
                                        </a>
                                    </li> 

                                    <li  class="nav-item">

                                        <a class="nav-link <?php if($uri == '/admin/mubes'){ echo'active'; } ?>
                                         "  href="/admin/mubes" >
                                        <i class="fas fa-fw fa-address-card "></i>
                                        <p>Mubes</p>
                                        </a>
                                    </li>
                                    <li  class="nav-item">
                                        <a class="nav-link <?php if($uri == '/admin/kontak'){ echo'active'; } ?> 
                                        " href="/admin/kontak">
                                        <i class="fas fa-fw fa-mobile "></i>
                                        <p>Kontak</p>
                                        </a>
                                    </li>
                                    <li  class="nav-header ">
                                        ACCOUNT SETTINGS
                                    </li>                                                                                                                                                                                                                                                                                                                                                                                                                                              <li  class="nav-item">
                                     <a class="nav-link <?php if($uri == '/admin/profil'){ echo'active'; } ?>"
                                         href="/admin/profil" >
                                            <i class="fas fa-fw fa-user "></i>
                                            <p> Profile </p>
                                     </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                    </aside>
<?php
}else if($na->id_roles == 3){
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
<a href="" class="brand-link ">
<img src="/assets/img/Logo-IAITI.png" alt="IAITI" class="brand-image img-circle elevation-3" style="opacity:.8">

    
    <span class="brand-text font-weight-light ">
        <b>IA</b>ITI
    </span>

</a>
    
    
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column " data-widget="treeview" role="menu">
            <li  class="nav-item">
                    <a class="nav-link <?php if($uri == '/admin/home'){ echo'active'; } ?> 
                    " href="/admin/home" >
                    <i class="fas fa-fw fa-tachometer-alt "></i>
                    <p>beranda<span class="badge badge-success right">Home</span></p>
                    </a>
                </li> 
                
				<li  class="nav-header ">

					MUBES MANAGEMENT

				</li>
				<li  class="nav-item">

					<a class="nav-link <?php if($uri == '/admin/listuser2'){ echo'active'; } ?>  "
					   href="/admin/listuser2"        >
					<i class="fas fa-fw fa-users "></i>
					<p> Users Mubes</p>
					</a>
				</li> 

				<li  class="nav-item">
					<a class="nav-link  <?php if($uri == '/admin/alumni'){ echo'active'; } ?> "
					   href="/admin/alumni"        >
						<i class="fas fa-fw fa-school "></i>
						<p>Users Alumni</p>
					</a>
				</li> 

				<li  class="nav-item">
					<a class="nav-link  <?php if($uri == '/admin/approved'){ echo'active'; } ?> "
					   href="/admin/approved"        >
						<i class="fas fa-fw fa-award "></i>
						<p> Waitting</p>
					</a>
				</li> 

                <?php /*
				<li  class="nav-item">
					<a class="nav-link  <?php if($uri == '/admin/schedule'){ echo'active'; } ?> "
					   href="/admin/schedule"        >
					   <i class="fas fa-fw fa-clock "></i>
						<p>Jadwal Mubes </p>
					</a>
				</li> 

				<li  class="nav-item">
					<a class="nav-link  <?php if($uri == '/admin/kandidat'){ echo'active'; } ?> "
					   href="/admin/kandidat"        >
						<i class="fas fa-fw fa-id-badge "></i>
						<p>Kandidat Mubes</p>
					</a>
				</li> 
                
                */ ?>
                <li  class="nav-header ">
                                        ACCOUNT SETTINGS
                                    </li>                                                                                                                                                                                                                                                                                                                                                                                                                                              <li  class="nav-item">
                                     <a class="nav-link <?php if($uri == '/admin/profil'){ echo'active'; } ?>"
                                         href="/admin/profil" >
                                            <i class="fas fa-fw fa-user "></i>
                                            <p> Profile </p>
                                     </a>
                                    </li>

            </ul>
        </nav>
     </div>
</aside>

<?php
}else{
echo'Saat Ini Anda Belum dapat Akses Apapun';
}
}
?>
        {{-- Content Wrapper --}}
        <div class="content-wrapper {{ config('adminlte.classes_content_wrapper') ?? '' }}">

            {{-- Content Header --}}
            <div class="content-header">
                <div class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
                    @yield('content_header')
                </div>
            </div>

            {{-- Main Content --}}
            <div class="content">
                <div class="{{ config('adminlte.classes_content') ?: $def_container_class }}">
                    @yield('content')
                </div>
            </div>

        </div>

        {{-- Footer --}}
        @hasSection('footer')
            @include('adminlte::partials.footer.footer')
        @endif

        {{-- Right Control Sidebar --}}
        @if(config('adminlte.right_sidebar'))
            @include('adminlte::partials.sidebar.right-sidebar')
        @endif

    </div>
@stop

@section('adminlte_js')
    @stack('js')
    @yield('js')
@stop

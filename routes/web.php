<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('beranda');
});


Route::get('/sendMail', [App\Http\Controllers\mailController::class, 'sendMail'])->name('sendMail');
Route::post('/recovery', [App\Http\Controllers\recoveryController::class, 'recovery'])->name('recovery');

Route::get('/detail/banner/{id}', [App\Http\Controllers\detailController::class, 'banner'])->name('banner');
Route::get('/detail/visi-misi', [App\Http\Controllers\detailController::class, 'vs'])->name('vs');
Route::get('/detail/so', [App\Http\Controllers\detailController::class, 'so'])->name('so');
Route::get('/detail/galery/{id}', [App\Http\Controllers\detailController::class, 'galery'])->name('galery');

Route::get('/DPT', [App\Http\Controllers\detailController::class, 'DPT'])->name('DPT');
Route::get('/tracer', [App\Http\Controllers\detailController::class, 'tracer'])->name('tracer');
Route::get('/kontak', [App\Http\Controllers\detailController::class, 'kontak'])->name('kontak');
Route::get('/mubes', [App\Http\Controllers\detailController::class, 'mubes'])->name('mubes');
Route::get('/Register-Mubes', [App\Http\Controllers\detailController::class, 'register'])->name('register');
Route::post('/registrasi/create', [App\Http\Controllers\detailController::class, 'create'])->name('register.create');

Auth::routes();
//Home
Route::get('/admin/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//banner
Route::get('/admin/banner', [App\Http\Controllers\BannerController::class, 'index'])->name('admin.banner.index');
Route::get('/admin/banner/add', [App\Http\Controllers\BannerController::class, 'add'])->name('admin.banner.add');
Route::post('/admin/banner/create', [App\Http\Controllers\BannerController::class, 'create'])->name('admin.banner.create');
Route::get('/admin/banner/show/{id}', [App\Http\Controllers\BannerController::class, 'show'])->name('admin.banner.show');
Route::get('/admin/banner/edit/{id}', [App\Http\Controllers\BannerController::class, 'edit'])->name('admin.banner.edit');
Route::post('/admin/banner/update/{id}', [App\Http\Controllers\BannerController::class, 'update'])->name('admin.banner.update');
Route::get('/admin/banner/destroy/{id}', [App\Http\Controllers\BannerController::class, 'destroy'])->name('admin.banner.destroy');

//tentang kami
Route::get('/admin/about', [App\Http\Controllers\aboutController::class, 'index'])->name('admin.about.index');
Route::post('/admin/about/create', [App\Http\Controllers\aboutController::class, 'create'])->name('admin.about.create');
Route::post('/admin/about/update/{id}', [App\Http\Controllers\aboutController::class, 'update'])->name('admin.about.update');
Route::get('/admin/about/vm', [App\Http\Controllers\aboutController::class, 'vm'])->name('admin.about.vm');
Route::post('/admin/about/vm_create', [App\Http\Controllers\aboutController::class, 'vm_create'])->name('admin.about.vm_create');
Route::post('/admin/about/vm_update/{id}', [App\Http\Controllers\aboutController::class, 'vm_update'])->name('admin.about.vm_update');
Route::get('/admin/about/so', [App\Http\Controllers\aboutController::class, 'so'])->name('admin.about.so');
Route::post('/admin/about/so_create', [App\Http\Controllers\aboutController::class, 'so_create'])->name('admin.about.so_create');
Route::post('/admin/about/so_update/{id}', [App\Http\Controllers\aboutController::class, 'so_update'])->name('admin.about.so_update');

//Tracer
Route::get('/admin/tracer', [App\Http\Controllers\tracerController::class, 'index'])->name('admin.tracer.index');
Route::post('/admin/tracer/create', [App\Http\Controllers\tracerController::class, 'create'])->name('admin.tracer.create');
Route::post('/admin/tracer/update/{id}', [App\Http\Controllers\tracerController::class, 'update'])->name('admin.tracer.update');

//Majalah
Route::get('/admin/majalah', [App\Http\Controllers\majalahController::class, 'index'])->name('admin.majalah.index');
Route::get('/admin/majalah/add', [App\Http\Controllers\majalahController::class, 'add'])->name('admin.majalah.add');
Route::post('/admin/majalah/create', [App\Http\Controllers\majalahController::class, 'create'])->name('admin.majalah.create');
Route::get('/admin/majalah/show/{id}', [App\Http\Controllers\majalahController::class, 'show'])->name('admin.majalah.show');
Route::get('/admin/majalah/edit/{id}', [App\Http\Controllers\majalahController::class, 'edit'])->name('admin.majalah.edit');
Route::post('/admin/majalah/update/{id}', [App\Http\Controllers\majalahController::class, 'update'])->name('admin.majalah.update');
Route::get('/admin/majalah/destroy/{id}', [App\Http\Controllers\majalahController::class, 'destroy'])->name('admin.majalah.destroy');

//Kategori 
Route::get('/admin/kategori', [App\Http\Controllers\kategoriController::class, 'index'])->name('admin.kategori.index');
Route::get('/admin/kategori/add', [App\Http\Controllers\kategoriController::class, 'add'])->name('admin.kategori.add');
Route::post('/admin/kategori/create', [App\Http\Controllers\kategoriController::class, 'create'])->name('admin.kategori.create');
Route::get('/admin/kategori/show/{id}', [App\Http\Controllers\kategoriController::class, 'show'])->name('admin.kategori.show');
Route::get('/admin/kategori/edit/{id}', [App\Http\Controllers\kategoriController::class, 'edit'])->name('admin.kategori.edit');
Route::post('/admin/kategori/update/{id}', [App\Http\Controllers\kategoriController::class, 'update'])->name('admin.kategori.update');
Route::get('/admin/kategori/destroy/{id}', [App\Http\Controllers\kategoriController::class, 'destroy'])->name('admin.kategori.destroy');

//galery 
Route::get('/admin/galery', [App\Http\Controllers\galeryController::class, 'index'])->name('admin.galeri.index');
Route::get('/admin/galery/add', [App\Http\Controllers\galeryController::class, 'add'])->name('admin.galeri.add');
Route::post('/admin/galery/create', [App\Http\Controllers\galeryController::class, 'create'])->name('admin.galeri.create');
Route::get('/admin/galery/show/{id}', [App\Http\Controllers\galeryController::class, 'show'])->name('admin.galeri.show');
Route::get('/admin/galery/edit/{id}', [App\Http\Controllers\galeryController::class, 'edit'])->name('admin.galeri.edit');
Route::post('/admin/galery/update/{id}', [App\Http\Controllers\galeryController::class, 'update'])->name('admin.galeri.update');
Route::get('/admin/galery/destroy/{id}', [App\Http\Controllers\galeryController::class, 'destroy'])->name('admin.galeri.destroy');

//kontak 
Route::get('/admin/kontak', [App\Http\Controllers\kontakController::class, 'index'])->name('admin.kontak.index');
Route::get('/admin/kontak/add', [App\Http\Controllers\kontakController::class, 'add'])->name('admin.kontak.add');
Route::post('/admin/kontak/create', [App\Http\Controllers\kontakController::class, 'create'])->name('admin.kontak.create');
Route::get('/admin/kontak/show/{id}', [App\Http\Controllers\kontakController::class, 'show'])->name('admin.kontak.show');
Route::get('/admin/kontak/edit/{id}', [App\Http\Controllers\kontakController::class, 'edit'])->name('admin.kontak.edit');
Route::post('/admin/kontak/update/{id}', [App\Http\Controllers\kontakController::class, 'update'])->name('admin.kontak.update');
Route::get('/admin/kontak/destroy/{id}', [App\Http\Controllers\kontakController::class, 'destroy'])->name('admin.kontak.destroy');

//mubes 
Route::get('/admin/mubes', [App\Http\Controllers\mubesController::class, 'index'])->name('admin.mubes.index');
Route::get('/admin/mubes/add', [App\Http\Controllers\mubesController::class, 'add'])->name('admin.mubes.add');
Route::post('/admin/mubes/create', [App\Http\Controllers\mubesController::class, 'create'])->name('admin.mubes.create');
Route::get('/admin/mubes/show/{id}', [App\Http\Controllers\mubesController::class, 'show'])->name('admin.mubes.show');
Route::get('/admin/mubes/edit/{id}', [App\Http\Controllers\mubesController::class, 'edit'])->name('admin.mubes.edit');
Route::post('/admin/mubes/update/{id}', [App\Http\Controllers\mubesController::class, 'update'])->name('admin.mubes.update');
Route::get('/admin/mubes/destroy/{id}', [App\Http\Controllers\mubesController::class, 'destroy'])->name('admin.mubes.destroy');

//roles 
Route::get('/admin/roles', [App\Http\Controllers\rolesController::class, 'index'])->name('admin.roles.index');
Route::get('/admin/roles/add', [App\Http\Controllers\rolesController::class, 'add'])->name('admin.roles.add');
Route::post('/admin/roles/create', [App\Http\Controllers\rolesController::class, 'create'])->name('admin.roles.create');
Route::get('/admin/roles/show/{id}', [App\Http\Controllers\rolesController::class, 'show'])->name('admin.roles.show');
Route::get('/admin/roles/edit/{id}', [App\Http\Controllers\rolesController::class, 'edit'])->name('admin.roles.edit');
Route::post('/admin/roles/update/{id}', [App\Http\Controllers\rolesController::class, 'update'])->name('admin.roles.update');
Route::get('/admin/roles/destroy/{id}', [App\Http\Controllers\rolesController::class, 'destroy'])->name('admin.roles.destroy');

//prodi 
Route::get('/admin/prodi', [App\Http\Controllers\prodiController::class, 'index'])->name('admin.prodi.index');
Route::get('/admin/prodi/add', [App\Http\Controllers\prodiController::class, 'add'])->name('admin.prodi.add');
Route::post('/admin/prodi/create', [App\Http\Controllers\prodiController::class, 'create'])->name('admin.prodi.create');
Route::get('/admin/prodi/show/{id}', [App\Http\Controllers\prodiController::class, 'show'])->name('admin.prodi.show');
Route::get('/admin/prodi/edit/{id}', [App\Http\Controllers\prodiController::class, 'edit'])->name('admin.prodi.edit');
Route::post('/admin/prodi/update/{id}', [App\Http\Controllers\prodiController::class, 'update'])->name('admin.prodi.update');
Route::get('/admin/prodi/destroy/{id}', [App\Http\Controllers\prodiController::class, 'destroy'])->name('admin.prodi.destroy');

//listuser 
Route::get('/admin/listuser', [App\Http\Controllers\listuserController::class, 'index'])->name('admin.listuser.index');
Route::get('/admin/listuser/cari', [App\Http\Controllers\listuserController::class, 'cari'])->name('admin.listuser.index');
Route::get('/admin/listuser2/cari2', [App\Http\Controllers\listuserController::class, 'cari2'])->name('admin.listuser2.index');
Route::get('/admin/listuser2', [App\Http\Controllers\listuserController::class, 'index2'])->name('admin.listuser2.index2');
Route::get('/admin/listuser/add', [App\Http\Controllers\listuserController::class, 'add'])->name('admin.listuser.add');
Route::get('/admin/listuser2/add2', [App\Http\Controllers\listuserController::class, 'add2'])->name('admin.listuser2.add2');
Route::post('/admin/listuser/create', [App\Http\Controllers\listuserController::class, 'create'])->name('admin.listuser.create');
Route::post('/admin/listuser2/create2', [App\Http\Controllers\listuserController::class, 'create2'])->name('admin.listuser2.create2');
Route::get('/admin/listuser/show/{id}', [App\Http\Controllers\listuserController::class, 'show'])->name('admin.listuser.show');
Route::get('/admin/listuser/edit/{id}', [App\Http\Controllers\listuserController::class, 'edit'])->name('admin.listuser.edit');
Route::get('/admin/listuser2/edit2/{id}', [App\Http\Controllers\listuserController::class, 'edit2'])->name('admin.listuser2.edit2');
Route::post('/admin/listuser/update/{id}', [App\Http\Controllers\listuserController::class, 'update'])->name('admin.listuser.update');
Route::post('/admin/listuser2/update2/{id}', [App\Http\Controllers\listuserController::class, 'update2'])->name('admin.listuser2.update2');
Route::get('/admin/listuser/destroy/{id}', [App\Http\Controllers\listuserController::class, 'destroy'])->name('admin.listuser.destroy');
Route::get('/admin/listuser2/destroy2/{id}', [App\Http\Controllers\listuserController::class, 'destroy2'])->name('admin.listuser2.destroy2');

//Profil
Route::get('/admin/profil', [App\Http\Controllers\listuserController::class, 'profil'])->name('admin.profil.index');
Route::post('/admin/profil/update_pro/{id}', [App\Http\Controllers\listuserController::class, 'update_pro'])->name('admin.profil.update_pro');

//Alumni 
Route::get('/admin/alumni', [App\Http\Controllers\alumniController::class, 'index'])->name('admin.alumni.index');
Route::get('/admin/alumni/cari', [App\Http\Controllers\alumniController::class, 'cari'])->name('admin.alumni.index');


//Approval 
Route::get('/admin/approved', [App\Http\Controllers\approvedController::class, 'index'])->name('admin.approved.index');
Route::get('/admin/approved/cari', [App\Http\Controllers\approvedController::class, 'cari'])->name('admin.approved.index');
Route::get('/admin/approved/cari2', [App\Http\Controllers\approvedController::class, 'cari2'])->name('admin.approved.index');
Route::get('/admin/approved/edit/{id}', [App\Http\Controllers\approvedController::class, 'edit'])->name('admin.approved.edit2');
Route::post('/admin/approved/update/{id}', [App\Http\Controllers\approvedController::class, 'update'])->name('admin.approved.update2');
Route::post('/admin/approved/all', [App\Http\Controllers\approvedController::class, 'all'])->name('admin.approved.all');
Route::get('/admin/approved/destroy/{id}', [App\Http\Controllers\approvedController::class, 'destroy'])->name('admin.approved.destroy');


//Approval 
Route::get('/admin/kandidat', [App\Http\Controllers\candidateController::class, 'index'])->name('admin.kandidat.index');
Route::get('/admin/kandidat/add', [App\Http\Controllers\candidateController::class, 'add'])->name('admin.kandidat.add');
Route::get('/admin/kandidat/cari', [App\Http\Controllers\candidateController::class, 'cari'])->name('admin.kandidat.index');
Route::get('/admin/kandidat/edit/{id}', [App\Http\Controllers\candidateController::class, 'edit'])->name('admin.kandidat.edit');
Route::post('/admin/kandidat/create', [App\Http\Controllers\candidateController::class, 'create'])->name('admin.kandidat.create');
Route::post('/admin/kandidat/update/{id}', [App\Http\Controllers\candidateController::class, 'update'])->name('admin.kandidat.update');
Route::post('/admin/kandidat/all', [App\Http\Controllers\candidateController::class, 'all'])->name('admin.kandidat.all');
Route::get('/admin/kandidat/destroy/{id}', [App\Http\Controllers\candidateController::class, 'destroy'])->name('admin.kandidat.destroy');
 
//Approval 
Route::get('/admin/schedule', [App\Http\Controllers\scheduleController::class, 'index'])->name('admin.schedule.index');
Route::get('/admin/schedule/cari', [App\Http\Controllers\scheduleController::class, 'cari'])->name('admin.schedule.index');
Route::get('/admin/schedule/edit/{id}', [App\Http\Controllers\scheduleController::class, 'edit'])->name('admin.schedule.edit2');
Route::get('/admin/schedule/add', [App\Http\Controllers\scheduleController::class, 'add'])->name('admin.schedule.add');
Route::post('/admin/schedule/create', [App\Http\Controllers\scheduleController::class, 'create'])->name('admin.schedule.create');
Route::post('/admin/schedule/update/{id}', [App\Http\Controllers\scheduleController::class, 'update'])->name('admin.schedule.update2');
Route::post('/admin/schedule/all', [App\Http\Controllers\scheduleController::class, 'all'])->name('admin.schedule.all');
Route::get('/admin/schedule/destroy/{id}', [App\Http\Controllers\scheduleController::class, 'destroy'])->name('admin.schedule.destroy');
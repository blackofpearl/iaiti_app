<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# iaiti_app
step by step
1. clone to local (git clone git@gitlab.com:blackofpearl/iaiti_app.git)
2. open directoy in command (cd '/d/PROJECT/new update/iaiti_app')
3. delete composer.lock in folder (git clean -f) 
4, Install composer (composer install)
5. buat .env dan setting .env sesuai DATABASE yang digunakan (cp .env.example .env)
6. generate key to .env (php artisan key:generate)
7. buat nama database nya, lanjut jalankan perintah migrasi database (php artisan migrate)
8. npm install
9. npm run dev
10. php artisan storage:link
11. php artisan serve
